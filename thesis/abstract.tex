The goal of computational biology is to build analytical models that accurately
reflect biological processes. One of the common tasks towards this goal is to
deduce relationships between entities that constitute a biological process using
an appropriate computational representation of these entities. For example, in
the study of evolutionary history of organisms, the key task is to construct a
phylogenetic tree, a tree representation of the evolutionary relationships among
a set of organisms, given their genome sequences. In many applications including
genome sequencing, clustering etc., the fundamental task is to identify the
sequences that are closely related to each other. In systems biology, the
grand-challenge problem is to construct a network that represents the
interactions between gene or gene products, given gene expression data collected
from a diverse range of experiments. In this thesis, our primary motivation is
to accelerate biological discoveries using large-scale datasets. In order to
scale the above mentioned tasks to big datasets, development of novel sequential
and parallel algorithms is necessary as most of the applicable methods take
quadratic time or longer. With an aim to accelerate biological discoveries, this
thesis presents scalable and efficient solutions to three problems, which lie at
the heart of the two fundamental areas in computational biology -- Approximate
Sequence Matching and Reverse-engineering Gene Networks.

In approximate sequence matching, the goal is to find matching sequences, while
allowing a small number of mismatches, insertions or deletions. We address two
related problems in Approximate Sequence Matching. The $\max \mathsf{lcp}_k$
problem is defined as follows: Given two sequences $\mathsf{X}$ and
$\mathsf{Y}$, with $|\mathsf{X}| + |\mathsf{Y}| = n$ and a mismatch threshold
$k \geq 0$, compute, for every $i, 1 \leq i \leq |\mathsf{X}|$, the longest
prefix of $\mathsf{X}$ starting at $i$ that matches with some substring of
$\mathsf{Y}$ while allowing $k$ mismatches. The first sub-quadratic algorithm
for this problem was proposed by Aluru, Apostolico and Thankachan (2015), which
takes $O(n \log^k n)$ time. Though this algorithm has the best-known theoretical
bounds, it uses many clever tricks and is difficult to implement. In this
thesis, we propose a simple, easier to implement algorithm that solves this
problem in $O(n \log^k n)$ expected time. We demonstrate the use of this
algorithm in the construction of phylogenetic trees using a distance metric evaluated from the lengths of the $k$-mismatch longest common prefixes.

We also address a closely related problem, the $k$-mismatch maximal common
substring problem, which is defined as follows: Given an input collection
$\mathsf{D}=\{S_1, \dots, S_m\}$ of $m$ sequences with $\sum_{i=1}^m S_i = N$, a
length threshold $\phi$ and a mismatch threshold $k \geq 0$, report all
$k$-mismatch maximal common substrings of length at least $\phi$ over all pairs
of sequences in $\mathsf{D}$. This problem is motivated by the need for sequence
matching algorithms that allow few mismatches so as to accommodate errors made
by sequencing machines. While many heuristic solutions have been proposed for
this problem, obtaining a provably efficient solution has remained elusive. In
this thesis, we present an algorithm with an expected run time
guarantee of $O(N\log^k N+\mathsf{occ})$, where $\mathsf{occ}$ is the output
size.

When $\mathsf{D}$ is a collection of millions of short DNA sequences as in the
case of high throughput sequencing datasets, there is a need for an efficient
parallel algorithm for this problem. Towards this goal, we present a novel
distributed memory parallel algorithm that runs in
$O\left(\left(\frac{N}{p}+\mathsf{occ}\right) \log^{k} N\right)$ expected time,
where $p$ is the number of processors, and takes only $O\left( \log^{k+1}
N\right)$ expected rounds of global communications, under some realistic
assumptions. To our knowledge, this is the first provably sub-quadratic time
parallel algorithm for solving this problem. We demonstrate the performance and
scalability of this algorithm using large high throughput sequencing datasets.

Reverse engineering gene networks from gene expression data is a widely studied
problem, for which numerous mathematical models have been developed. However,
few methods can scale to large number of experiments at the genome-scale, and to
date, comparative assessment of network reconstruction methods has not been
conducted at the genome-scale. To address this, we analyze 11,760 microarray
experiments on the widely-studied model plant \emph{Arabidopsis thaliana} drawn
from public repositories. Using this data, we generate genome-scale networks of
Arabidopsis using three different methods -- Pearson correlation, mutual
information and Gaussian graphical modeling. We analyze and compare these
networks to test for their robustness in successfully recovering relationships
between functionally related genes. Our comparisons include benchmarking against
experimentally confirmed interactions, the Arabidopsis network resource AraNet,
and study of specific pathways. As our comparitive study shows, mutual
information based construction methods are favored because of their ability to
recover non-linear relationships and low algorithmic complexity. Finally, we
present the first ever construction of mutual information based genome-scale
gene networks using MapReduce. We develop a scalable solution for all the stages
of the network construction algorithm using only the \textit{map}
and \textit{reduce} operations. Our implementation using \textit{Apache Spark}
can scale with the number of virtual instances, and can be used to construct
networks of sizes in the range of 2000 to 5000 genes within an hour in a cost
effective manner. We demonstrate the capability to construct genome-scale
networks by reverse engineering a network of over 17,000 genes.


\textbf{Keywords:}  Approximate Sequence Matching, Gene Networks, Arabidopsis Thaliana,
Microarray data processing
