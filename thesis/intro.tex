\chapter{Introduction}
\label{chapter:intro}
The primary goal in Computational Biology is to construct
\emph{in-silico} models that can accurately reproduce biological
processes. However, most of these models limit their scope either to
only a small sub-set of the data or to a fewer number of variables in
the problem domain. The limited scope of these models are mostly due to
the computational requirements imposed by currently available methods
for large scale problems. We are interested in expanding the limits of
these computational methods either by proposing novel methods and
algorithms or by developing cost-effective and easy-to-deploy
alternatives.

Towards this goal, we focus on two major problem areas in Computational
Biology. They are approximate sequence matching and reverse-engineering
gene networks.  We describe our motivations for each of these problems
in \sectionref{intro.motiv}. In
\sectionref{intro.contrib}, we discuss our contributions in each of
these problems. Finally in \sectionref{intro.outline}, we provide an
outline for the thesis. But first, we discuss the fundamental concepts
of molecular biology in \sectionref{intro.concepts} since all the
computational problems of our interest arise from this domain.


\section{Foundations of Molecular Biology}
\label{section:intro.concepts}

In this section, we introduce the fundamental concepts in molecular biology,
which sets up the context for all the problems of our interest. Specifically, we
discuss the Central Dogma of Molecular Biology, proposed by Crick and Watson
about half a century ago~\cite{Watson1953}. The Central Dogma was proposed as an
explanation about the role of genes in biological processes and how they express
themselves as proteins. Although many discoveries since then have shown that
gene expression is a much more complicated process than the view proposed by the
Central Dogma, it is mostly true today. What follows is simplified explanation
of \emph{gene expression}, which is sufficient for our purposes.
(\figureref{gene.regulation.expression}).

\begin{figure}[!htb]
\begin{center}
  \includegraphics[width=\textwidth]{./graphics/graphics-expression.pdf}
\end{center}
  \caption[Central Dogma of Molecular Biology]{
    Expression of a fragment of a gene is shown. In the transcription
    step, mRNA is constructed from DNA sequence. In the translation step,
    with the help of tRNA, protein molecules are constructed from
    mRNA. Leucine, Serine and Alanine are aminoacids corresponding
    to the sequences GAG, UCA and CGG respectively.
  \label{figure:gene.regulation.expression}
  }
\end{figure}

\textit{Genome} is an organism's complete set of genetic information.
It is stored inside each cell as the molecule \textit{DNA}. DNA has two
strands, with each strand being a chain or sequence of nucleotides,
called bases -- Adenine (A), Guanine (G), Cytosine(C), and
Thymine(T). Adenine bonds with Thymine and Guanine bonds with Cytosine
making the two strands of DNA complementary.

The length of the genome can range from a few hundred thousand base pairs to
tens of billions of base pairs, depending upon the organism. For example, the
length of yeast bacteria's genome is $\approx12.1 \times 10^6$ base pairs, while
the length of the human genome is $\approx3.3 \times 10^9$ base pairs.
Genome size does not necessarily correspond to the organism's complexity as some
single cell organisms have longer genome than humans.

A genome comprises of several hundred to thousands of
\textit{genes}. Genes are the basic unit of biological function that is
passed from one generation to the next. Genes carry the instructions to
make molecules called \textit{proteins} that are essential for all
biological functions.  The structure of a gene is defined by the
sequence of bases (or base pairs) in its DNA strand.

Gene expression is the process by which the information in a gene's
DNA sequence is used to synthesize proteins. A \textit{protein} molecule
consists of one or more polypeptides, which are chains of
aminoacids. Proteins are the most structurally variable and therefore
the most functionally versatile of the bio-polymers.

During gene expression, another molecule called the \textit{RNA} plays
an important role. RNA is a single stranded polymer of nucleotides,
whose bases are -- Adenine (A), Guanine (G), Cytosine (C) and Uracil
(U). Since RNA is single stranded, it has greater structural freedom
than DNA. While DNA, whose regular structure is suited for long-term
storage and replication of genetic information, RNA assumes more active
roles in expressing this information.

Gene Expression proceeds in two steps: \textit{Transcription} and
\textit{Translation}. During transcription, one of the two strands of
the DNA serves as the template and a complementary strand of RNA is
synthesized from this template. The transcribed RNA is known as
\textit{messenger RNA} or mRNA in short, because it carries the same
genetic message as the gene. Translation is the process by which a
protein molecule is assembled using the genetic information of mRNA. A
cellular particle called \textit{Ribosome} performs translation by
mapping each set of a three consecutive bases in mRNA to a specific
aminoacid, and then linking the aminoacids to construct the protein. As
an example, \figureref{gene.regulation.expression} shows the basepair
triplets \texttt{CUC}, \texttt{AGU} and \texttt{GCC} being translated to
the aminoacids Leucine, Serine and Alanine respectively.
% Since mRNA is the product of transcription, mRNAs are also called
% \textit{transcripts} and the set of all mRNAs is called
% \textit{transcriptome}.

A gene's expression can be controlled by other genes. A gene which can
control the expression of another is called a \textit{regulatory gene}.
One of the many ways a regulatory gene controls its target gene's
expression is by controlling the transcription process. Transcription
starts at a site known as the \emph{promoter region} of a gene's DNA
sequence. The DNA sequence at this particular site is recognized by
specific set of proteins known as \textit{transcription
  factors}. Abundance or absence of transcription factors can switch on
or off the expression of the target gene. Apart from transcription
factors, activator and suppressor proteins, which bind to different
parts of the DNA, also regulate transcription. The transcription
factors, activator and suppressor proteins are the products of
expression of regulatory genes. For a gene to express itself, it
requires that the corresponding regulatory genes are also expressed.

A plethora of technologies have been developed to study the various
aspects of these biological processes. We focus on two such technologies
-- Sequencing and Microarrays. They produce two different types of data
leading to many different types of computational problems. Sequencing
experiments, as the name implies, generates sequences \ie string data,
while Microarrays produce a complete snapshot of expression levels,
quantitatively measured by floating point values. We provide a brief
overview of these technologies and describe how the biological questions
of interest are posed as computational problems, whose input is the data
generated from these experiments.

\subsubsection{Sequencing}
\label{sssection:sequencing}

Given that an organism's genome contains the complete set of genetic
information, finding the sequence of an organism's complete genome is of
utmost importance to the biologists. However, due to the limitations of
current technologies, sequencing machines can not read the entire genome
of an organism. Sequencing machines can, however, read small contiguous
stretches of a DNA molecule and produce output sequences with lengths
ranging from few hundred to few thousand base pairs. Sequencing machines
also can read from the same region of a genome many times over by
creating multiple copies of the input DNA molecule.

The output sequences generated by sequencing machines are called
\emph{reads}. Due to the repeated sampling by the sequencing machine
from the same region of the source DNA molecule, a position in the
source DNA sequence will be covered by multiple reads generated in the
same sequencing experiment. The average number of times a position in
the genome is read is called the \textit{coverage}. If the source DNA
molecule is uniformly sampled, then coverage is computed as ratio of the
total length of all the reads from an experiment to the total length of
the DNA molecule being sequenced.

Depending upon the technology used by the sequencing machine, the length of the
reads, the quality of the reads, and the throughput per run vary. The technology
used by sequencing machines can be broadly classified into two categories --
before and after high-throughput sequencing. Prior to the advent of
high-throughput sequencing in the late 2000s, Sanger technologies were
predominantly used for sequencing DNA molecules. When they were at their
cheapest, Sanger sequencing machines were able to produce 84 kilobases per run
at a cost of US\$ 5000 per kilobase~\cite{Shendure2008}. With the advent of
high-throughput sequencing (HTS) technologies~\cite{Bentley2008}, sequencing
machines today have exponentially grown in throughput, while significantly
reducing the cost~\cite{Reuter2015, Goodwin2016}. The latest HTS machines from
Illumina Corporation, the market leader of HTS machines, can produce 1.8
terabases per run at the cost of less than US\$1 per
gigabase~\cite{Illumina2016}.

However, HTS technologies have a few disadvantages. The reads from HTS
machines are shorter in length and have a higher error rate compared to
the Sanger based sequencing technologies. While reads from Sanger-based
technologies were able to produce reads of few thousand bases of length,
the latest of the Illumina machines are limited to maximum read length
of less than 200 base pairs. Also, while the errors generated in Sanger
sequencing machines were negligible, reads from Illumina machines have
error rate around 1\%~\cite{Nakamura2011}.

Given the above mentioned limitations of sequencing machines, the problem of
recovering the genome of an organism can be reduced to (a) finding the
relationship between reads sampled from different locations across the genome,
then (b) identifying the order of the reads w.r.t to the genome, and finally (c)
stitching the reads together to recover the complete genome. This problem of
recovering the genome of an organism from an input set of reads is called
\emph{de novo Genome Assembly}~\cite{Pop2009}.

Apart from \textit{de novo} assembly, sequencing technologies have many other
applications such as Resequencing~\cite{Li2010, Topol2007},
Metagenomics~\cite{Wooley2010} and RNA-Seq~\cite{Wang2009}. Each of these
applications address a different biological question. In Resequencing, the
genome of an organism is already known and the goal is to find the location from
which an input read originates from. In RNA-Seq, instead of sequencing the
genome, the mRNAs are sequenced and the goal is to study the gene expression
levels of all the genes. In Metagenomics, the DNA sequenced comes from genomes
of a diverse population of micro-organisms rather than from a genome of a single
organism. Here, the goal is to partition the sequences into clusters such that a
cluster consists only of the sequences from the same micro-organism.

The common theme in all of these sequence problems is the following :
Given a set of input sequences, the task is to determine how these
sequences are related to each other. This particular aspect of sequence
problems and the need to handle errors produced from multiple different
sources motivates our first set of problems --- approximate sequence
matching with $k$ mismatches, which we discuss further
in \sectionref{intro.motiv.approx}.

\subsubsection{Microarrays}
\label{sssection:marrays}

Based on the basic understanding of gene expression explained earlier in
this section, one can deduce that the larger the amount of mRNAs
available, the higher the number of protein molecules produced. This
intuition mostly holds true, although there are many factors such as
environmental conditions and other interference mechanisms that control
gene expression. In general, measuring the mRNA levels can be used as a
proxy for measuring the expression levels of the corresponding genes.

One of the widely used technologies to measure gene expression levels is DNA
Microarrays~\cite{Schulze2001}. A microarray experiment captures a snapshot of
genome-wide gene expression levels and produces as its output, a two dimensional
image of probes intensities, each probe corresponding to a gene. The output
images show the probe intensity values proportional to the expression levels of
the corresponding gene -- higher the intensity of a probe, higher the expression
levels of the gene corresponding to the probe. Raw probe intensity values,
corresponding to every gene in the genome, are then normalized with respect to
the control samples, and are reported as a quantitative measure of gene
expression levels.

The biological question we focus in this thesis is how effectively can
one recover the relationships between the genes using the gene
expression data collected from a large collection of Microarray
experiments. As mentioned earlier, genes interact with each other in
complex ways. If two genes interact with each other, one expects that
the expression levels of the two genes correlate with each other either
positively or negatively. The goal in reverse-engineering gene networks
is to deduce these interactions from gene expression data. We discuss
this problem further in
 \sectionref{intro.motiv.networks}.

\section{Motivation}
\label{section:intro.motiv}

In this thesis, the primary motivation is to accelerate biological discoveries
using large-scale datasets. However, in order to scale the most applicable
analysis methods to big datasets, development of novel sequential and parallel
algorithms is necessary since most of these methods take quadratic time or
longer. Unfortunately, even in problems that arise from related fields, there
are significant differences in the challenges presented and hence, they require
different techniques. Towards achieving our primary goal, this thesis presents
efficient and scalable solutions to three crucial problems lying at the heart of
the two key areas in computational biology -- approximate sequence matching and
reverse-engineering genome-scale gene networks.


\subsection{Approximate Sequence Matching}
\label{section:intro.motiv.approx}

As noted in the previous section (p.~\pageref{sssection:sequencing}),
sequence matching is fundamental to many applications such as \textit{de novo}
Assembly, Metagenomics, and RNA-Seq.  We focus on two problems in
approximate sequence matching. The unifying theme between these two
problems is finding common substrings between two sequences while
allowing $k$ mismatches. In this section, we motivate these problems by
discussing a few applications related to this theme.  While our
discussions focus on a few specific applications, the computational
problems we address are posed in a general manner so that our solutions
can also be used in other string matching applications.

% All life on earth shares a common ancestor, which lived more than 3.5
% billion years ago.

In the evolution of biological populations, \emph{speciation} is defined as the
process by which biological populations, which are isolated reproductively \ie
mate only with other members of the same population, gradually evolve to form
new species. On the genome level, this appears as multiple changes in the genome
sequence over a significant period of time, with each change affecting only a
small number of genes, leading to genetic divergence, and hence, speciation.
Phylogenetic analysis~\cite{Freckleton2002} aims to deduce these changes,
reconstruct the correct evolutionary history, and estimate the time of
divergence between species. The result of such analysis is a tree representation
of the evolutionary relationships between the species. An example of a
phylogenetic tree is shown in \figureref{approx.primates.tree}.
Given $r$ biological species, phylogenetic analysis attempts to
reconstruct the evolutionary history of $r$ species based on their
genome or protein sequences. In order for any phylogenetic analysis to
be accurate, it is critical that the estimation of evolutionary distance
between two species from their respective sequences is accurate.

One popular approach for evolutionary distance estimation relies on
sequence alignment. In a sequence alignment of two sequences $S_1$ and
$S_2$, the goal is to determine similar regions of $S_1$ and $S_2$ by
optimizing a function that penalizes the cost of edit operations
required to change $S_1$ into $S_2$. An example output of sequence
alignment for two sequences is shown below.
\begin{center}
\begin{tabular}{ll}
$S_1$ & \texttt{TTTAAT-CAGGTAT-} \\
$S_2$ & \texttt{-TTAATTCTGGTATT} \\
\end{tabular}
\end{center}
The above example shows that the sequence $S_1$ can be changed to $S_2$ by the
following four edit operations: (i) Delete $S_1$'s first character \texttt{T},
(ii) Insert the character \texttt{T} after the sixth character of $S_1$, (ii)
Replace $S_1$'s seventh character \texttt{A} by \texttt{T}, and (iv) Append the
character \texttt{T} to $S_1$. Sequence alignment problems can be solved with
dynamic programming in $O(n^2)$ time, where $n$ is maximum length of the
sequences.

A typical pipeline for alignment-based phylogenetic inference of $r$
species performs a multiple sequence alignment of $r$ sequences and it
proceeds as follows. First, an all-to-all pairwise sequence alignment is
performed to construct a $r \times r$ distance matrix for the input
sequences. The evolutionary distance between two sequences in the matrix
is inferred from the output of an alignment, \eg{} equal to one minus
percent identity in the alignment. Second, a guide tree is constructed
from the pairwise distance matrix and then multiple sequences are
progressively aligned in the order determined by the guide
tree. Finally, the phylogenetic tree is inferred from the resulting
multiple sequence alignments using a tree inference algorithm, which can
be distance-, parsimony- or likelihood-based. Note that it is also
possible to construct a phylogenetic tree directly from the $r \times r$
pairwise distance matrix computed in the first step, using some
distance-based tree construction algorithm such as unweighted pair group
method with arithmetic mean (UPGMA) \cite{Sokal1958} and
neighbor-joining (NJ) \cite{Saitou1987}.

Alignment-based approaches, while accurate, involve high computational costs.
This is because pairwise alignment uses dynamic programming and takes time
complexity that is quadratic to the sequence lengths. The difficulty is further
exacerbated when constructing the phylogenetic tree for a large number of
sequences, especially long sequences (\eg{} eukaryotic genomes). Given a
collection of $r$ sequences of average length $n$, the time complexity for
pairwise distance matrix computation can be as high as $O(r^2n^2)$ when using
pairwise alignment. As a computationally efficient alternative, $l$-mer based
approaches have become popular~\cite{Vinga2003, Leimeister2014, Blaisdell1989,
  Wu2001, Edgar2004}. Here, the distance between two sequences are measured
using alignment-free exact $l$-mer (a $l$-mer is a string of $l$ characters)
counting. $l$-mer counting computations can be done in $O(r^2n)$ time,
significantly reducing the run-time by a factor of $n$.

In order to overcome the accuracy problems in $l$-mer based approaches without
having to spend significant computational cost, the Average Common Substring
(ACS) metric has been proposed~\cite{Ulitsky2006, Leimeister2014b} as a measure
of evolutionary distance between two species. Given two sequences, the ACS
method first calculates the length of the longest common substring that starts
at each position $i$ in one sequence and matches any substring of the other
sequence. Then, it averages and normalizes all of the lengths computed to
represent the similarity of the two sequences. Finally, the resulting similarity
value is used to compute the pairwise distance. Using string data structures we
describe in \sectionref{approx-defn}, ACS can be computed in time proportional
to the sum of the lengths of the two sequences.
Approximate sequence matching
offers an improvement over the ACS, by introducing $k$ mismatches. Here, the
first is step is to calculate the length of the longest common substring
starting at each position $i$ in one sequence and matches any substring of the
other sequence within a Hamming Distance of $k$. A solution to the $\max
\mathsf{lcp}_k$ problem can be used to compute this measure, which in trun can
be used to construct phylogenetic trees. We discuss our contributions to this
application in \sectionref{intro.contrib}.

Our next application of approximate sequence matching algorithms relates to
analyses of reads from sequencing experiments. As mentioned earlier, modern
sequencing instruments produce a large collection of short reads that are
randomly drawn from one or multiple genomes. Deciphering pairwise alignments
between the reads is often the first step in many applications. For example, one
may be interested in finding all pairs of reads that have a sufficiently long
overlap, such as suffix/prefix overlap (for genomic or metagenomic
assembly~\cite{Simpson2012}), or substring overlap (for read
compression~\cite{Fritz2011}, finding RNA sequences containing common
exons~\cite{Grabherr2011, Sakarya2012}, etc). Much of modern-day high-throughput
sequencing is carried out using Illumina sequencers, which have a small error
rate ($<$ 1-2\%) and predominantly ($> 99\%$) substitution
errors~\cite{Nakamura2011}. Thus, algorithms that tolerate a small number of
mismatch errors can yield the same solution as the much more expensive alignment
computations. Motivated by such applications, we formulate the \textbf{all pairs
  $k$-mismatch maximal common substrings problem} or $k$MCS problem. In both
sequential and parallel settings, many approaches based on filtering heuristics
have been proposed~\cite{Burkhardt2003, Kucherov2014, Valimaki2012, Simpson2012,
  Kalyanaraman2003, Scheetz2005, Sarje2013} for this problem. We present both
sequential and parallel algorithms with expected time guarantees for this
problem. We discuss this problem and our contributions in
\sectionref{intro.contrib}.

\subsection{Genome-scale Gene Networks}
\label{section:intro.motiv.networks}

As noted in the previous section (p.~\pageref{sssection:marrays}), we are
interested in the construction of models that capture the interactions between
the genes with better fidelity and in a cost-effective manner. Even though these
construction methods are used to infer genome-scale network models from large
gene expression data sets, comparative studies of different gene network
reconstruction methods, and their performance against ground truth, have been
limited to small network sizes and/or experiments~\cite{Soranzo2007,
  Marbach2010, Marbach2012}. In this work, we attempt to rectify this by
studying the genome-scale network construction methods in the context of the
organism of our interest, \emph{Arabidopsis Thaliana}. Here, we (a) collect all
the \emph{Arabidopsis ATH1} microarray data available in the public gene
expression databases, (b) construct networks using the methods applicable for
genome-scale that have been previously shown to work at
genome-scale~\cite{Mao2009, Ma2007, Aluru2013}, (c) analyze these networks using
seed genes from well-studied pathways, and (d) validate the generated networks
against experimentally verified interactions~\cite{Jin2015}.

In our compartive study, Mutual Information (MI) based network construction
methods produce high quality networks at genome-scale compared to other methods.
However, for genome-scale gene networks of organisms with tens of thousands of
genes, they still require significant investment in computational
equipment~\cite{Zola2010}. We address this problem by developing a
MapReduce-based solution to construct genome-scale MI-based networks. Such
solutions can provide small research labs the ability to expand their network
analysis capabilities without having to commit to a long-term investment on
computational resources.

\section{Contributions}
\label{section:intro.contrib}

In both of these problem areas, we extend the limits of existing methods
in different ways. In approximate sequence matching, we introduce novel
algorithms and solutions in identifying relationship between the
sequences.

We use the following notations. For a sequence $X$, its length is
denoted by $|X|$, $i$th character by $X[i]$ and sub-string that starts
at position $i$ and ends at position $j$ by $X[i\dots j]$.  We use $X_i$
or $X[i\dots]$ to denote the suffix of $X$ starting at location $i$. We
define $\mathsf{lcp}_k(X_i, Y_i)$ to denote the longest prefix of $X_i$
that matches with a prefix of $Y_j$ within Hamming distance $k$.

We introduce efficient algorithms with expected runtime guarantees for
two problems -- what we term as the $\max \mathsf{lcp}_k$ problem and the $k$MCS
problem. The $\max \mathsf{lcp}_k$ problem is defined as follows:

\vspace{10pt}
\noindent\fbox{
\begin{tabularx}{0.96\textwidth}{rl}
  \textbf{Problem} & $\max \mathsf{lcp}_k$ problem \\
  \toprule
  \textbf{Given} & Two strings $X$ and $Y$, with $|X| + |Y| = n$ and a mismatch threshold $k \geq 0$  \\
  \textbf{Goal} & Compute the array $\lambda$, where $\lambda[i] =
  \max_j |\mathsf{lcp}_k(X_i,Y_j)|$
\end{tabularx}
}
\vspace{10pt}

For the $\max \mathsf{lcp}_k$ problem, we introduce an $O(n \log^k n)$
expected time solution. As an application of our $\mathsf{lcp}_k$
algorithm, we use the $\mathsf{lcp}_k$ values for constructing
phylogenetic trees as follows: First, we compute the ACS distance
measure with Hamming Distance $k$ between every pair sequences (derived
from the organism's genomes) and then, we construct a tree based on the
pairwise distances. We show that the trees constructed using this method
are just as good as the ones produced from the computationally expensive
Multiple Sequence Alignment solution. The following publications are the
result of our work in $\max \mathsf{lcp}_k$ algorithm and its
application to phylogenetic tree construction.
\begin{enumerate}
\item Sharma Thankachan, \textbf{Sriram P. Chockalingam}, and Srinivas Aluru.
  ALFRED: a practical method for alignment-free distance computation.
  \textit{Journal of Computational Biology}, 23(6):452-460, 2016.
%% \item Sharma Thankachan, \textbf{Sriram P. Chockalingam}, Yongchao Liu, Ambujam
%%   Krishnan and Srinivas Aluru. A greedy alignment-free distance estimator for
%%   phylogenetic inference. In \textit{Proceedings of the 5th IEEE International
%%     Conference on Computational Advances in Bio and Medical Sciences (ICCABS)},
%%   2015.
\item Sharma Thankachan, \textbf{Sriram P. Chockalingam}, Yongchao Liu, Ambujam
  Krishnan and Srinivas Aluru. A greedy alignment-free distance estimator for
  phylogenetic inference. \textit{BMC Bioinformatics}, Vol. 18 Suppl. 9, 2017.
\end{enumerate}

Though Sharma Thankachan leads as the first author in both of the above
publications, this work is included in this thesis as it helps in
introducing the fundamental ideas of our approximate sequence matching
work and the relationship between the two problems of our focus. Also,
this work includes significant contributions from the author of this
thesis in the implementation of the algorithm and phylogenetic analysis.

The second problem of our interest, what we term as the $k$MCS problem,
or the \textbf{$k$-mismatch maximal common substring} problem is defined as
follows.

\vspace{10pt}
\noindent\fbox{
\begin{tabularx}{0.96\textwidth}{rl}
\textbf{Problem} & $k$MCS problem                                                           \\
\toprule
\textbf{Given}   & A database of $m$ strings $D = \{S_1, \ldots, S_m\}$, $\sum_i |S_i| = N$ \\
                 & a length threshold $\phi$, and a mismatch threshold $k \geq 0$           \\
\textbf{Goal}    & Find all $k$-mismatch maximal common substrings of length at least $\phi$     \\
                 & between any pair of strings in $D$.
\end{tabularx}
}
\vspace{10pt}

We adapt our algorithm for $\max \mathsf{lcp}_k$ to solve the above
problem in $O(N \log^k N + \mathsf{occ})$ expected time, where
$\mathsf{occ}$ is the output size. The massive size of high throughput
sequencing datasets necessitate efficient parallel approximate sequence
matching algorithms. In order to handle such datasets, we developed a
parallel algorithm for the $k$MCS problem. Under a realistic assumption
(described in \sectionref{approx-parallel-algo-kmcsn}) applicable for
most High-throughput sequencing datasets, the parallel algorithm takes
$O\left(\left(\frac{N}{p} + \mathsf{occ} \right) \log^k N \right)$
expected time and takes only $O\left( \log^{k+1} N\right)$ expected
rounds of global communications, where $p$ is the number of
processors. We demonstrate the scalability and performance of our
parallel algorithm using both genomic and gene expression datasets
ranging in size from 18 million to over 270 million reads, on up to 1024
processor cores. The following publications result from our work in the
$k$MCS problem.

% We present an efficient distributed memory parallel algorithm that
% solves this all-pair approximate maximal common substrings problem in
% $O\left((\frac{N}{p}+\occ)\; \log^{k} N\right)$ expected time using
% $O\left( \log^{k+1} N\right)$ expected communication rounds, under the
% reasonable assumption that the frequency of occurrence of any
% $\tau$-long substring across all sequences in $\FD$ is no more than
% $\frac{N}{p}$. Under this assumption, our algorithm enforces an
% effective partitioning of a series of modified suffix trees to localize
% processing within each processor.

\begin{enumerate}
\item \textbf{Sriram P. Chockalingam}, Sharma Thankachan and
  Srinivas Aluru. A Parallel Algorithm for $k$-Mismatch All-pair
    Maximal Common Substrings. In
  \textit{Proceedings of the International Conference for High Performance Computing, Networking,
  Storage and Analysis, SC 2016}, pages 784--794, 2016.
\item Sharma Thankachan, \textbf{Sriram P. Chockalingam}, and Srinivas Aluru. An
  Efficient Algorithm for finding all pairs $k$-mismatch maximal common
  substrings. In \textit{Proceedings of the 12th International Symposium on
    Bioinformatics Research and Applications (ISBRA)}, 2016.
\end{enumerate}

In case of Genome-scale gene networks, our comparative study of network
construction methods represents the first genome-scale comparative
study, using \emph{Arabidopsis thaliana} microarray datasets, knowledge
of specific biological pathways, and validation using experimentally
confirmed interactions. Additionally, our reference set of networks
constructed from various reverse-engineering methods also serves as a
useful resource for biologists interested in a specific pathway or in
identifying novel functions of genes.

We also extended the existing capabilities of network
reverse-engineering by introducing a cost-effective and easy-to-deploy
solution. We developed an MI-based network construction solution using the
MapReduce framework and demonstrated its efficiency and cost
effectiveness by constructing a 17,000 gene network using \emph{Amazon
  EC2} instances.  This work resulted in the following publications.
\begin{enumerate}
\item \textbf{Sriram P Chockalingam}, Maneesha Aluru, Hongqing Guo, Yanhai Yin,
  and Srinivas Aluru. Reverse Engineering Gene Networks: A Comparative Study at
  Genome-scale. In \textit{Proceedings of the 8th ACM International Conference
    on Bioinformatics, Computational Biology, and Health Informatics(ACM-BCB)},
  pages 480--490, 2017.
\item
  \textbf{Sriram Chockalingam}, Maneesha Aluru, and Srinivas Aluru. Microarray
  Data Processing Techniques for Genome-Scale Network Inference from Large
  Public Repositories. \textit{Microarrays}, 5(3):23, 2016.
\item \textbf{Sriram P Chockalingam}, Maneesha Aluru and Srinivas Aluru.
  Information Theory Based Genome-scale Gene Networks Construction using
  MapReduce. In \textit{Proceedings of the 22nd IEEE International Conference on
    High Performance Computing (HiPC)}, pages 464--473, 2015.
\end{enumerate}

\section{Outline}
\label{section:intro.outline}

We discuss our work on approximate sequence matching in
\chapterref{approx} and reverse-engineering networks in
\chapterref{networks}.


\section{Credits}
\label{section:intro.credits}
The work on $\max \mathsf{lcp}_k$ problem was done in collaboration with Sharma
Thankachan, an Assistant Professor at the University of Central Florida.
His main contribution to the work is the development of sequential algorithm,
and my contributions are implementation of sequential algorithm, development of
phylogenetic inference using the $\mathsf{ACS}_k$ metric, conducting experiments
for phylogenetic inference. Other contributors in the work include Yangchao Liu,
 a Research Scientist at Ant Financial, and Ambujam Krishnan. They
helped in datasets collection and construction of reference phylogenetic trees.

For the $k$MCS problem, development of the sequential algorithm was done
in collaboration with Sharma Thankachan. In case of the parallel
algorithm for the $k$MCS problem, both its development and
implementation of the parallel algorithm were done by the author of this
thesis. Sharma helped in resolving a few issues in the
correctness of the algorithm and in preparing the manuscript for
publication.

Prof. Maneesha Aluru at the Department of Biology at Georgia Institute
of Technology is the main collaborator for the work on Gene
Networks. Her contributions include classification of microarray data,
and analysis of the generated networks based on seed genes from
well-studied pathways. The author's contribution in this work are in
development of the MapReduce algorithm and its implementation,
collection and pre-processing of data, analysis of data classification,
and preparation of metrics related to the network analysis. Prof. Yanhi
Yin at the Iowa State University performed the experiments on the novel
interactions as suggested by the reverse-engineered networks.
