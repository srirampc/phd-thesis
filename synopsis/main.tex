\include{abbrev}
\vspace{-2cm}
\section{Introduction}
\label{section:intro}

The primary goal in Computational Biology is to construct
\emph{in-silico} models that can accurately reproduce biological
processes. However, most of these models limit their scope either to
only a small subset of the data or to a fewer number of variables in
the problem domain. The scope limits are mostly due to the computational
requirements imposed by currently available methods for large scale
problems. We are interested in extending the limits of these
computational methods either by proposing novel methods and algorithms
or by developing cost-effective and easy-to-deploy alternatives. Towards
this goal, we focus on two major problem areas in Computational
Biology. They are Approximate Sequence Matching and Reverse-engineering
gene networks.

Sequence matching problems lie at the heart of many applications in
computational biology. The \emph{Genome}, an organism's complete set of
genetic information, is typically modeled as sequence of characters
generated from the alphabet set $\{A, C, G, T\}$, representing the
constituent nucleotides of the DNA molecule. Hence, sequence matching is
fundamental to many applications such as denovo Assembly, Metagenomics,
and RNA-Seq. In approximate sequence matching, the task is to match or
align sequences while allowing a small number of errors. We focus on two
problems in approximate sequence matching. The unifying theme between
these two problems is finding common substrings between two sequences
while allowing $k$ mismatches. We discuss these two problems, our
contributions to these problems and an application using our proposed
algorithm in \sectionref{approx}.

In reverse-engineering gene networks, one is interested in the
construction of network models that capture the interactions between the
genes with better fidelity and in a cost-effective manner. Even though
an important use of network methods is to apply them to learn
genome-scale networks from large gene expression data sets, comparative
studies of different gene network reconstruction methods, and their
performance against ground truth, have been limited to small network
sizes and/or experiments.  In this work, we attempt to rectify this by
studying the genome-scale network construction methods in the context of
the organism of our interest, \emph{Arabidopsis Thaliana}.  Our results
from the comparative study show that Mutual Information (MI) based
network construction methods produce high quality networks compared to
other construction methods. MI-based network construction still requires
significant investment in computational equipment for genome-scale gene
networks of organisms with tens of thousands of genes. We address this
problem by developing a MapReduce-based solution to reverse engineer
genome-scale MI-based networks. Such solutions can provide small
research labs the ability to expand their network analysis capabilities
without having to commit to a long-term investment on computational
resources.  We discuss our contributions in this area
in \sectionref{networks}.


\section{Approximate Sequence Matching}
\label{section:approx}

Most sequence matching problems can be modeled as the computation of
edit distance, for which dynamic programming algorithms (eg. sequence
alignment) provide solutions in quadratic time and linear space. With
the advent of high-throughput sequencing and hence datasets with
hundreds of millions of reads, these dynamic programming solutions have
become computationally infeasible. Fortunately, sequence matching is
typically used to identify closely related sequences. This typical use
case has led to development of many filtering algorithms that use
heuristics based on exact matching. For example, the input sequences are
first scanned for smaller fixed length strings, called $l$-mers, ($l$
being the length of the strings) and then an alignment based solution is
used only if two sequences share significant percentage of
$l$-mers. However, these exact matching based heuristics do not provide
any guarantees on run-time or the output.

Approximate sequence matching hits a sweet spot, as a compromise between
alignment based solutions and exact matching based heuristics. In
approximate sequence matching problems, one attempts to match two or
more sequences while allowing $k$ mismatches or insertions or
deletions. In this section, we define the approximate string matching
problems we focus on in \sectionref{approx-problems}. We provide a brief
overview of previous approaches
in \sectionref{approx-overview}. Finally, we discuss our contributions
to these problems and its application to phylogenetic analysis
in \sectionref{approx-contributions}.

\subsection{Problem Definitions}
\label{section:approx-problems}

We use the following notations: For a sequence $\X$, its length is
denoted by $|\X|$, $i$th character by $\X[i]$ and sub-string that starts
at position $i$ and ends at position $j$ by $\X[i \dots j]$.  For
brevity, we use $\X_i$ to denote the suffix of $\X$ starting at location
$i$. We define $\lcp_k(\X_i, \Y_i)$ to denote the longest prefix of
$\X_i$ that match with a prefix of $\Y_j$ within Hamming distance
$k$. We define the following approximate string matching problem, which
we refer to as $\max \lcp_k$ problem in the rest of the synopsis.

\begin{problem}
\label{prob:lcpk}
\emph{Approximate String Matching -- $\max \lcp_k$ problem}

\vspace{10pt}
\noindent\fbox{
\begin{tabularx}{0.96\textwidth}{rl}
  \textbf{\emph{Problem}} &
 \emph{$\max \lcp_k$ problem} \\
  \toprule
  \textbf{\emph{Given}} &
 \emph{Two strings $\X$ and $\Y$, with $|\X| + |\Y| = n$ and a mismatch threshold $k \geq 0$}  \\
  \textbf{\emph{Goal}} &
 \emph{Compute the array $\lambda$, where $\lambda[i] =  \max_j |\lcp_k(\X_i,\Y_j)|$}
\end{tabularx}
}
\vspace{10pt}
\end{problem}
The primary motivation of this problem stems from its application to
phylogenetic tree construction, where the $|\lcp_k|$ values are used to
estimate the evolutionary distance between two organisms from their
corresponding sequences. We discuss this application further
in \sectionref{approx-overview}.

We also address a related problem, the $k$-mismatch maximal common
substring problem ($k$MCS problem), which occurs in many applications in
processing large set of sequences such as Error Correction, denovo
Genome Assembly, Metagenomics, and RNA-Seq. Motivated by these
applications and considering the error characteristics of the commonly
used high throughput sequencing machines, we state the following
approximate string matching problem, which we call the $k$MCS problem in
the rest of the synopsis.

\begin{problem}
\label{prob:kmcsn}
\emph{Approximate String Matching -- $k$MCS problem for $m$ strings}

\vspace{10pt}
\noindent\fbox{
\begin{tabularx}{0.96\textwidth}{rl}
  \textbf{\emph{Problem}} & \emph{$k$MCS problem} \\
  \toprule \textbf{\emph{Given}} &
  \emph{A database of $m$ strings $\FD = \{S_1, \ldots, S_m\}$, $\sum_i |S_i| = N$} \\
  & \emph{a length threshold $\phi$, and a mismatch threshold $k \geq 0$} \\
  \textbf{\emph{Goal}} &
  \emph{Find all \textbf{$k$-mismatch maximal common substrings} of length at least $\phi$} \\
  & \emph{between any pair of strings in $D$.}
\end{tabularx}
}
\vspace{10pt}
\end{problem}

\subsection{State of the Art Approaches}
\label{section:approx-overview}

$|\lcp_k(\X_i, \Y_i)|$ can be defined in a recursive manner as follows.
\begin{equation}
\label{equation:approx-lcpk}
  |\lcp_k(\X_i, \Y_j)|   = \left\{
  \begin{array}{l l}
    z &
      \quad \textnormal{if $k=0$}\\
    z+1+|\lcp_{k-1}(\X_{i+z+1}, \Y_{j+z+1})| & \quad \textnormal{if $k >0$}
  \end{array} \right.
\end{equation}
where $z=|\lcp(\X_i,\Y_j)|$.  The above equation provides a dynamic
programming solution for this problem, which takes $O(n^2)$ space and
$O(n^2k)$ time. Since a quadratic solution is not practical for longer
sequences, many heuristic solutions with no run-time guarantees have
been proposed~\cite{Leimeister2014}. Aluru, Apostolico and
Thankachan(AAT) presented an $O(n \log^k n)$ that works for any constant
$k$~\cite{Aluru2015}. AAT algorithm relies on the results proved by
Sleator and Tarjan in~\cite{Tarjan1981}, known as the heavy path
decomposition. AAT algorithm uses heavy path decomposition to extend the
generalized suffix tree by carefully selecting a bounded set of modified
suffixes.

AAT's algorithm was motivated by the application of $\lcp_k$ based
metrics to phylogenetic analysis. Given $r$ biological species,
phylogenetic analysis attempts to reconstruct the evolutionary history
of $r$ species based on their genome or protein sequences. In order for
any phylogenetic analysis to be accurate, it is critical that the
estimation of evolutionary distance between two species computed from
their respective sequences is accurate.

Recently, the Average Common Substring ($\ACS$) metric has been
proposed~\cite{Leimeister2014b} as a measure of evolutionary distance
between two species. Given two sequences, the ACS method first
calculates the length of the longest common substring that starts at
each position $i$ in one sequence and matches any substring of the other
sequence. Then, it averages and normalizes all of the lengths computed
to represent the similarity of the two sequences. Finally, the resulting
similarity value is used to compute the pairwise distance between the
sequences. Using suffix tree data structures, $\ACS$ can be computed in
time proportional to the length sum of the two sequences.

Approximate Sequence Matching offers an improvement over the $\ACS$, by
introducing $k$ mismatches. Here, one computes the length of the longest
common substring starting at each position $i$ in one sequence and
matches any substring of the other sequence while allowing $k$
mismatches \ie $\max_j |\lcp_k(\X_i, \Y_j)|$.  The $k$-mismatch average
common substring of $\X$ w.r.t. $\Y$, denoted denoted by $\ACS_k(\X,
\Y)$ is defined as follows,
\begin{equation}
\label{equation:similarity}
\ACS_k(\X, \Y) = \frac{1}{|\X|}\sum_{i=1}^{|\X|}\lambda[i]
\end{equation}
where $\lambda[i] = \max_j |\lcp_k(\X_i,\Y_j)|$. Since computing
$\ACS_k$ had a quadratic time complexity prior to AAT's
algorithm,~\cite{Leimeister2014b} and~\cite{Horwege2014} used a greedy
heuristics to approximate $\ACS_k$ and use this approximation to
estimate pairwise distance of $\X$ and $\Y$, in an alignment-free
manner, for phylogenetic tree reconstruction.

With respect to the second problem of our focus -- the $k$MCS problem,
all prior attempts are heuristics that neither provide guarantees on the
run-time nor guarantee the inclusion of all pairs that should be part of
the output.  Heuristic solutions first select promising pairs via some
filtering approach and then verify them via alignment. In the sequential
setting, they can be broadly classified under three categories: suffix
filtering~\cite{Kucherov2014, Valimaki2012}, spaced seeds
filtering~\cite{Burkhardt2003} and substring
filtering~\cite{Simpson2012}. In case of parallel heuristic methods,
previous work was mainly focused on corresponding
applications~\cite{Kalyanaraman2003,Scheetz2005}. There is also work
done on accelerating pairwise distance estimations among $n$ sequences
which can be applied to this problem~\cite{Sarje2013}. However, with
short sequences and low error rate (of mostly mismatches), sequence
alignment can impractical because of its quadratic time complexity.

\subsection{Contributions}
\label{section:approx-contributions}

Although AAT's algorithm ~\cite{Aluru2015} has the best known
theoretical bounds for $\max \lcp_k$ problem (Problem~\ref{prob:lcpk}),
it is difficult to implement because it uses many clever tricks to
carefully select bounded set of suffixes. We proposed a simpler and easy
to implement algorithm for this problem. While the worst case time
complexity of our practical algorithm is exponential in $n$, its
expected time complexity is $O(n \log^k n)$. We used our solution to
compute $\ACS_k$ and used it to construct phylogenetic
trees~\cite{Thankachan2015a}. Our results show that the trees
constructed using this method are just as good as the ones produced from
the computationally expensive Multiple Sequence Alignment
solution. Since our algorithm is exponential in $\log n$ in the expected
case, we also introduced a greedy heuristic for longer sequences based
on out $\max \lcp_k$ algorithm. Our experimental results show that this
heuristic is faster than the $\max \lcp_k$ algorithm and works
reasonably well for longer sequences~\cite{Thankachan2015b}.

For the $k$MCS problem (Problem~\ref{prob:kmcsn}), we adapt our
algorithm for $\max \lcp_k$, to solve the above problem in $O(N \log^k N
+ occ)$ expected time~\cite{Thankachan2016}.  We also propose an
efficient distributed memory parallel algorithm for the same problem,
under some realistic assumptions applicable for most High-throughput
sequencing datasets. This algorithm takes $O\left((\frac{N}{p}+\occ)
  \log^{k} N\right)$ expected time and only $O\left( \log^{k+1}
  N\right)$ expected rounds of global communications where $p$ is the
number of processors and $\occ$ is the output size. To our knowledge,
this is the first provably sub-quadratic time algorithm for solving this
problem.  We demonstrate the performance and scalability of our
algorithm using large high throughput sequencing data sets.

\section{Genome-scale Gene Networks}
\label{section:networks}

In this section, we discuss our work on reverse-engineering gene
networks at the genome scale. The primary objective here is to construct
a network model, which truly reflects the interactions between all the
genes of an organism. A genome-scale network has vertexes representing
all the genes of the organism and an edge in the network is interpreted
as a potential direct interaction between two genes. Input for the
reverse-engineering problem is gene expression data, generated by
microarray or RNA-Seq experiments.

We represent the given set of $n$ genes as $\{g_1,\ldots,g_n\}$. As
stated above, the goal is to construct a network representing the
interactions among these $n$ genes. The input gene expression data is
available to us as $D$, an $n \times m$ matrix of floating point
values. The matrix $D$ contains the observed expression values of the
$n$ genes in $m$ different experiment settings. We term row $i$ of the
matrix $D$ as the \textbf{gene expression profile} of gene $g_i$. The
problem of network construction is formally defined as follows: Given an
expression profile matrix $D$, construct a network that represents the
interactions among the $n$ genes, and thus provide the most apt
explanation for our observation matrix $D$.

This section is organized as follows. In \sectionref{networks.overview},
we provide a overview of the network construction methods, and
in \sectionref{networks.contributions}, we discuss our contributions.


\subsection{Overview of Network Construction Methods}
\label{section:networks.overview}

Reverse-engineering gene networks is a well-studied problem in Systems
Biology and many different mathematical models have been proposed (See
~\cite{Marbach2012} for a recent review).  Till date, only three
network construction methods have been shown to work at the genome-scale
-- Pearson Correlation networks, Mutual Information-based networks and
Gaussian Graphical Models.

Even though Bayesian networks have been shown to produce better results
for gene networks with respect to analysis and
discovery~\cite{Friedman2004, Sachs2005}, their application at
genome-scale has been achieved only through restrictive heuristics and
with the aid of massively parallel, highest-end
supercomputers~\cite{Misra2014}. This is because learning Bayesian
Networks is NP-Hard~\cite{Chickering1995}.  Many prediction methods such
as Random Forests~\cite{Irrthum2010} and LASSO~\cite{Haury2012} have
also been proposed for gene network construction at small scale (less
than two hundred genes), but their effectiveness in large-scale network
construction have not yet been studied. In section, we briefly overview
the three methods applicable at genome-scale.

Co-expression networks are among the simplest models of gene networks,
which are constructed by evaluating the Pearson correlation coefficient
for every pair of genes in the genome.  Due to its simplicity, Pearson
correlation coefficient based methods have been widely used to build
large-scale networks for many different organisms, specifically to
discover functional modules~\cite{Stuart2003, Mao2009}. While it is
computationally efficient to construct such a network for large numbers
of genes, the major drawback of Pearson correlation is that it can only
infer linear, pairwise correlations.

A Gaussian Graphical model (GGM) is an undirected graph representation
of a joint Gaussian distribution of $n$ random variables corresponding
to the expression values of the genes.  To construct a GGM of a gene
network, Schafer~\textit{et al.}~\cite{Schafer2005} developed an
analytic shrinkage estimator to estimate the partial correlation
coefficient measure in an efficient manner. While such models have been
shown to be able to construct large-scale gene networks~\cite{Ma2007},
they still only recover linear relationships between genes.

Mutual information (MI) based methods, as the name implies, compute MI
values between every pair of genes and then report statistically
significant correlations as the edges in the output network.  MI-based
methods differ in their choice of how MI is calculated and how the
threshold for statistical significance is estimated. One of the most
popular MI-based methods called ARACNe~\cite{Basso2005} uses a
Gaussian-kernel based estimator to compute MI and selects a threshold
based on equation from large deviation theory. Another method called
CLR~\cite{Faith2007} computes MI using B-Spline based
estimator~\cite{Daub2004} and uses a mixture of Gaussian distributions
to select statistically significant edges. \emph{TINGe}~\cite{Zola2010}
permits the construction of large scale networks using supercomputers
and MPI-based clusters.


\subsection{Contributions}
\label{section:networks.contributions}

Comparative studies of different gene network reconstruction methods,
and their performance against ground truth, have been limited to small
network sizes and/or experiments. In addition, many studies were
performed \emph{in-silico} with synthetic networks and synthetically
generated datasets, for convenience of validation.  However, it has been
shown that using the same network inference methods, quality of
resulting networks from real data are not as good when compared to
simulation studies~\cite{Allen2012}.  In our work, we rectified these
shortcomings by studying the genome-scale network construction methods
in the context of the model plant, \emph{Arabidopsis Thaliana}.  We (a)
collected all the \emph{Arabidopsis ATH1} microarray data available in
the public gene expression databases, (b) constructed networks using the
methods applicable for genome-scale that have been previously shown to
work at genome-scale, (c) analyzed these networks using seed genes from
well-studied pathways, and (d) validated the generated networks against
experimentally verified interactions.

Our work represents the first comparative study of network construction
methods at genome-scale, using \emph{Arabidopsis Thaliana} microarray
datasets, knowledge of specific biological pathways, and validation
using experimentally confirmed interactions. Additionally, our reference
set of networks constructed from various reverse-engineering methods
also serve as a useful resource for biologists interested in a specific
pathway or in identifying novel functions of genes.

Results of our comparative study showed that MI-based network
construction methods produce high quality networks at genome-scale
compared to other methods. However, constructing MI-based networks at
genome scale for organisms with tens of thousands of genes still
requires significant investment in computational equipment.  Our
contribution here is to extend the existing capabilities of network
reverse-engineering by introducing a cost-effective and easy-to-deploy
solution. We introduced a MapReduce-based solution to reverse engineer
genome-scale MI-based networks.  We developed an MI-based network
construction solution using the {\it Spark} framework and demonstrated its
efficiency and cost effectiveness by constructing a 17,000 gene network
using \emph{Amazon EC2} instances~\cite{Chockalingam2015}.

\section{Conclusions}
\label{section:conclusions}
Approximate sequence matching based approaches have been increasingly
used for large-scale sequence comparison, since they are much faster
compared to alignment-based approaches. In this work, we presented a new
and practical algorithm, which is able to identify the exact substring
in sequence $\Y$ that appears as a prefix of any given suffix in
sequence $\X$ within Hamming distance $k$. This algorithm has an
expected time complexity of $O(n\log^kn)$ and is easy to implement in
practice. To demonstrate its usage in an application, we employed this
algorithm for phylogenetic analysis. Considering the generality of our
algorithm, we expect that a variety of biological applications, such as
metagenomic classification/clustering, sequence alignment and genome
assembly, could benefit from this work. Future work might involve
adapting the $\max \lcp_k$ algorithm for these applications and also in
developing greedy heuristics that is suitable for the corresponding
application.

Based on our algorithm for $\max \lcp_k$, we developed a new parallel
algorithm for $k$-mismatch maximal common substring problem. Our
parallel algorithm achieves an expected parallel run-time complexity of
$O\left((\frac{N}{p}+\occ) \log^{k} N\right)$, where $\occ$ is the
number of such reported maximal common substrings. We present our
algorithm for the practical distributed memory model of parallel
computation, and demonstrate its performance on real, large-scale
datasets. While the scaling results are constrained by the size of the
parallel computer available to us, we see no difficulty for the
algorithm to scale beyond the 1,024 cores it is demonstrated on.  We
anticipate the use of our algorithm for diverse applications in genome
mapping and assembly for high throughput datasets.

Our work on comparitive study of reverse-engineered Genome-scale
networks have shown that networks generated by the Mutual Information
based method \emph{TINGe} have better characteristics in terms of
functional modularity as measured by both connected component and
sub-network extraction analysis with respect to gene sets selected from
Brassinosteroid and stress regulation. In the future, one valuable
proposition is to make search and analysis of such a combination of
networks easily accessible to biologists.

We also presented a method to construct mutual information based
genome-scale gene networks and demonstrated its application on the model
plant \textit{ Arabidopsis Thaliana}. Our method works well even for
medium-scale networks in the range of 2000 to 5000 genes with moderate
requirements in cost and computing power. Solutions provided by
rent-on-demand clusters such as Amazon EC2, Google Compute Engine, and
Microsoft Azure have become attractive to many small research
laboratories because they do not require dedicated parallel-computing
resources.  We anticipate that our implementation will deliver large
gene network construction capabilities to many research group.
