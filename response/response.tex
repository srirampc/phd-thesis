\documentclass[11pt,article,oneside]{memoir}

\usepackage{graphicx,url}
\usepackage{rotating}

\usepackage[american]{babel}
\usepackage[babel,autostyle]{csquotes}
\usepackage[abbreviate=true,backend=biber]{biblatex}
\usepackage{palatino}
\usepackage{mathpazo}

%% Name, Title, Affiliation, Contact. Change as needed.
\def\myaffiliation{Dept. of Computer Science and Eng., \\ Indian Institute of Technology Bombay.}
\def\myauthor{Sriram Ponnambalam C}
\def\mynumber{(Roll. No.: 10405602)}
\def\myemail{\small{\texttt{\href{mailto:srirampc@cse.iitb.ac.in}{srirampc@cse.iitb.ac.in}}}}
\def\mytitle{Response to Refrees' Comments}
\def\mythesis{ \textbf{Parallel Algorithmic Techniques for Large-Scale Problems} \\ \textbf{in Computational Biology}}
\def\myadvisor{}
\def\mykeywords{}

\usepackage[usenames,dvipsnames]{color}
\usepackage[%xetex,
  colorlinks=true,
  urlcolor=BlueViolet, % external links
  citecolor=BlueViolet, % citations
  filecolor=BlueViolet, % local files
  plainpages=false,
  pdfpagelabels,
  bookmarksnumbered,
  pdftitle={\mytitle},
  pdfauthor={\myauthor},
  pdfkeywords={\mykeywords}
]{hyperref}

\counterwithout{section}{chapter}
\bibliography{response}
\begin{document}
\title{\mytitle}

\author{\mythesis{} \\ \\ \myauthor \\ \mynumber{} \\ \myaffiliation{} \\ \myemail{}} % \\ \\ \myadvisor{} }% \thanks{I am grateful to }

\maketitle

In this report, I address all the referees' comments on the submitted thesis. In section~\ref{sec1}, I address the issues raised in both the referee reports . In sections~\ref{sec2} and~\ref{sec3}, the comments specifically mentioned by Referees 1 and 2 are addressed, respectively.

\section{Response to Common Comments}
\label{sec1}

The common issues raised by both the referees will be addressed as follows:
\begin{enumerate}
  \item
\textbf{Typographical and Grammatical Errors:}
Both the referees have noted many typographical and grammatical errors in the thesis.

\textit{Response:}
I regret these errors and will fix them in the final version of the thesis.

\item
\textbf{Clarification of Definitions in Abstract:}
Both the referees have pointed out the use of undefined notations and acronyms in the abstract.

\textit{Response:}
I will revise the abstract such that no unfamiliar notations or acronyms are used.

\item
\textbf{Contributions in Abstract:}
Both the referees have noted that the abstract does not enumerate the specific contributions made by the submitted thesis.
Referee 1 also noted the inconsistency between the contributions described in abstract and those listed in Chapter 1.

\textit{Response:}
To make the abstract consistent with Chapter 1, I will revise the abstract to enumerate all the contributions.

\item
\textbf{Related Work in Chapter 1:}
Both the referees have noted the lack of citations to the related work in the first chapter, where the problems of interest are introduced.

\textit{Response:}
I have cited the key related works in the later chapters (Sections 2.5, 2.7, and 3.1).
I will follow the referees' suggestion and update the Sections 1.2.1 and 1.2.2 with the appropriate citations.

\item
\textbf{Conclusion Chapter :}
Both the referees have noted that the thesis seem to abruptly end at Chapter 3 and emphasized the need for a final conclusion chapter that summarizes the results and discusses future work and research directions.

\textit{Response:}
Since the thesis covers two different but related research areas, I added  separate conclusion sections for each of these areas.
However, I agree with the referees' rationale for a final conclusion chapter and will add one following their suggestions.
\end{enumerate}

\section{Response to Referee 1 (Prof. Yogesh Simmhan)}
\label{sec2}
The specific comments by Referee 1 will be addressed as follows:

\begin{enumerate}
\item
\textbf{Synergy between the Research Areas (Chapter 1):}
Referee 1 suggests an optional addition of a paragraph that describes how the thesis' contributions in two loosely related research areas (Approximate String Matching and Gene Networks) hang together.

\textit{Response:}
W.r.t. the synergy between the key contributions presented in the thesis, I will add the following in chapter 1:
In this thesis, the primary motivation is to accelerate biological discoveries using large-scale datasets.
However, in order to scale the applicable data analysis methods to big datasets, development of novel sequential and parallel algorithms is necessary as most of these methods take quadratic time or longer.
Unfortunately, even in problems that arise from related fields, there are significant differences in the challenges presented and hence, they require different techniques.
Towards achieving our primary goal, this thesis presents scalable solutions to some of the crucial problems lying at the heart of the two key areas in computational biology -- Approximate Sequence Matching and Reverse-engineering Gene Networks.


\item
\textbf{Algorithm Descriptions for Compact Suffix Tries (Chapter 2):}
Referee 1 suggests that the operations defined on the compact trie of suffixes be described in detail in Section 2.1.4.

\textit{Response:}
Following the referee's suggestion, I will add algorithm descriptions for the operations on compact trie of suffixes in Section 2.1.4, similar to the descriptions provided in Section 2.1.3 for the suffix tree operations using suffix and LCP arrays.

\item
\textbf{Plots for Complexity Analysis based Runtimes (Chapter 2):}
Referee 1 suggests that it would be useful to include, in the runtime plots (Figures 2.5 and 2.13), the runtime bounds predicted by the complexity analysis.

\textit{Response:}
I agree with the referee's suggestion and will additional scatter plots showing the estimated runtime bounds.

\item
\textbf{ALFRED-G vs. kmacs (Chapter 2):}
Referee 1 writes, ``ALFRED-G is occasionally better in quality but consistently slower than Kmacs for the 3 datasets considered.''

\textit{Response:}
This observation is incorrect. Figures 2.7 and 2.8 show that for \textit{27 primates} and \textit{Roseobacter} datasets,  the quality of ALFRED-G's output is better in terms of the measured RF-distance compared to that of the kmacs output, though ALFRED-G is slower than kmacs. Note that lower the RF-distance of the output tree w.r.t. the reference tree, better the quality of the output. Only for the \textit{BaliBASE} dataset, kmacs performs slightly better compared to ALFRED-G in some cases.

\item
\textbf{Reduction of null-key Values (Chapter 3):}
In the call to the reduce operation in the Permutation Testing task (Page 101), the argument passed is a key-value pair with null-key: $\langle null, [w_1, \ldots, w_q] \rangle$.
The referee notes that this implies that the a reduce operation is applied on $O(n^2)$ values for every pair $(i, j), 1 \leq i < j \leq n$.

\textit{Response:}
Clearly, this is not the intent of the algorithm. In the thesis, I mention, ``We use the null key to indicate that the reduce operation is preformed across all the items generated from the map operation''.
Note that the map operation generates as output a $q$-length array per pair, $(i, j), 1 \leq i < j \leq n$.
However, in the task description (Page 101; Section 3.3.2), the $null$ key inputs appear to indicate that a single reduce operation is applied to $O(n^2)$ inputs per pair.
To rectify this, I will change the input to reduce operation (Page 101; Section 3.3.2) to accept as input, only the $q$-length arrays of the MI-values computed in the previous step i.e., $[w_1, \ldots, w_q]$.
The use of $null$ key is present only in the description of the algorithm, not in the actual implementation.

\item
\textbf{Parallel Efficiency Plots (Chapter 3):}
Referee 1 writes, ``It is not clear that you run plant on 4 nodes in Fig. 3.5''.

\textit{Response:}
Fig. 3.5 shows, for both the flower and whole--plant datasets, the plots of the runtime against 4, 8, 16, 32, 64 and 128   instances.
Reading this information from the plot seems to be difficult, probably because the points corresponding to 4 and 8 instances appear too close to each other.
I will redo the plot in logscale so that the data points are clearly identifiable from the plots.

\item
\textbf{Cost Efficiency Discussion (Chapter 3):}
Referee 1 notes that the discussion related to cost efficiency is not clear.

\textit{Response:}  In Section 3.4.4, I will add the following to the discussion about cost efficiency :
In case of infrastructure-as-service (\textit{IaaS}) vendors such as \textit{Amazon EC2} and \textit{Microsoft Azure}, the user is charged cost per instance hour.
Also, these costs do not include the time to setup the instances and install the software.
For example, a program which takes an hour to setup and ten minutes to run is more expensive than one which takes three hours to setup and less than a second to run.
Therefore, the gains due to parallel efficiency should remain proportional to the total economic costs incurred by adding more instances.
To incorporate the economic models of \textit{IaaS} vendors, the thesis discusses, in addition to the runtime analysis, the efficiency of our proposed algorithm in terms of the economic costs required for the additional instances.

\item
\textbf{\textit{c3.2xlarge} v. \textit{c3.8xlarge} Performance (Chapter 3):}
W.r.t. the discussion on the performance of the Map-Reduce solution in Section 3.4.5, referee 1 writes, ``Why would there be contention when the data structure being read is `read only'? ''.

\textit{Response:}
In Section 3.4.5, Table 3.9 reports the runtimes for two different types of instances available with Amazon EC2: \textit{c3.2xlarge} and \textit{c3.8xlarge}.
A \textit{c3.2xlarge} instance has 2 vCPUs (virutal CPUs), whereas a \textit{c3.8xlarge} instance has 32 vCPUs. For our application, we assign one thread per vCPU.
For the same number of total threads, \textit{c3.2xlarge} shows better performance than \textit{c3.8xlarge} in our experiments.
For example, when run with 128 threads, our algorithm runs $\approx$2.5X faster with 16 \textit{c3.2xlarge} instances (i.e., $16 \times 8$ threads) than four \textit{c3.8xlarge} instances (i.e., $4 \times 32$ threads).
In both cases, most of the time is spent during permutation testing.
The explanation given in the thesis for this discrepancy is the contention between the threads, while reading the read-only data matrix.
As pointed out by the referee, this explanation is incorrect.

I will update the explanation as follows:
From \textit{Amazon EC2} website~\cite{AmazonEC2}, we learn that the servers used for running both these instances have Intel Xeon E5-2680 v2 processors and a vCPU is provisioned for each hyperthread.
Intel Xeon E5-2680 v2 has 10 physical cores and can accommodate 20 hyperthreads.
Since, we do not know the complete details on how the virtual instances are provisioned, the difference in runtime is probably due to a combination of the following factors.
\begin{itemize}
\item
  \textit{c3.2xlarge} instance is provisioned 8 vCPUs, and therefore, all the 8 vCPUs are more likey be allocated within one physical processor.
  However, in case of a \textit{c3.8xlarge}, provisioning 32 vCPUs requires at least two physical processors.
\item
  Muti-threaded applications generally show inconsistent performance with hyperthreading.
%\item
%  Work loads of other virtual instances that share the same hardware could influence the runtime.
\item
  \textit{c3.2xlarge} and \textit{c3.8xlarge} instances have different interconnect with different bandwidth.
  While \textit{c3.8xlarge} instances are connected via a 10Gbit Ethernet, the details of \textit{c3.2xlarge}'s interconnect are not available.
\item
  A worker process in a \textit{c3.2xlarge} and a \textit{c3.2xlarge} instance manages 8 and 32 executor threads, respectively -- leading to differences in the time spent for scheduling tasks and communicating results.
  %With instances,  , whereas a worker processor in a \textit{c3.8xlarge} instance manages 32 threads.
\end{itemize}
For this application, the proposed algorithms perform better with large number of \textit{c3.2xlarge} instances than fewer number of \textit{c3.8xlarge} instances.

%During this phase, all the threads access the read-only data matrix and there is little data dependency between the threads.
%Theoretically, there should be no contention for multiple threads to access read-only data.
%However, read-only multi-thread performance depends upon the data access patterns and the architecture.
%If the data matrix is larger than the available cache, the requests for access to the same data elements by two different threads may require co-ordination via the bus.
%Also, they way the task of computing $w_t$ is distributed, two threads with rank $j$ and $j+1$ would most likely require the same row from the data matrix.
%This explains the discrepancy in runtime performance.

\item
\textbf{Novel Interactions (Chapter 3):}
Referee 1 writes that the following statement in Page 116 is unclear: ``Our networks have already been used to a few confirm novel interactions.''

\textit{Response:}
I agree with the referee that this sentence is confusing. I will clarify it as follows.
Based on the interactions suggested by the generated networks, Prof. Yanhai Yin's research group at Iowa State University designed wet-lab experiments that confirmed novel gene interactions in \textit{Arabidopsis Thaliana}.

\item
\textbf{Repetition of References to Future Sections:}
I will remove any repetitive references of problems solved in a later sections.

\end{enumerate}

\section{Response to Referee 2 (Prof. Ananth Kalyanaraman) }
\label{sec3}

The specific comments by Referee 2 will be addressed as follows:
\begin{enumerate}
\item
\textbf{Citation in Introduction:}
I will follow the referee's recommendation and update the citation to~\textcite{Thankachan2016} in the introduction chapter.

\item
\textbf{Abbreviation for ``Aluru, Apostolico and Thankachan'' Algorithm: }
The referee has noted that the use of ``AAT'' as the acronym for the algorithm proposed by~\textcite{Aluru2015} is not appropriate.

\textit{Response:}
Since this acronym appears only in Abstract and Section 2.3, I will replace this acronym with a reference to the appropriate citation.

\item
\textbf{Definition of Compact Suffix Trie: }
Referee 2 comments that the term ``Compact Suffix Trie'' is confusing.

\textit{Response:}
In the thesis, the ``Compact Suffix Trie'' is used to describe the data structure constructed in the proposed $\max \mathsf{lcp}_k$ algorithms.
This term was used because the data structure is a compact trie of a carefully chosen subset of the suffixes.
Since \textit{trie} and \textit{compact trie} are standard terms used in suffix tree literature, I agree with the referee's concern that the term ``Compact Suffix Trie'' may be confusing for a reader.
To avoid this confusion, I will replace the uses of  ``Compact Suffix Trie''  to ``Compact Trie of Suffixes''.

% we use with the standard terminology used in suffix-tree literature, the data structure built
% In the algorithms proposed in the thesis we call them a \textit{compact suffix trie} because we are building a compact tries of a suffixes.
%is a trie in which the edge labels are strings instead of characters

\item
\textbf{Logscale Plots (Figs. 2.4 and 2.5): }
As recommended by the referee, I will redo the plots in Figures 2.4 and 2.5 in logscale.

\end{enumerate}

\section*{References}
\printbibliography[heading=none]

\end{document}
