\section{Approximate Sequence Matching}

\subsection{$\max$ $\lcp_k$ Problem}
\begin{frame}
  \frametitle{ Approximate Sequence Matching -- $\max \lcp_k$ Problem}
  %\vspace*{0.2cm}
    \begin{center}
      \begin{itemize}\itemsep=4ex
        \vspace*{0.1cm}
      \item Given
        \begin{itemize} \itemsep=2ex
          \vspace*{0.1cm}
        \item Two sequences $\X$ and $\Y$, drawn from the alphabet set $\Sigma$, s.t. $|\X| + |\Y| = n$,
        \item Mismatch threshold $k \geq 0$.
        \end{itemize}
      \item Output $\lambda$, array of length $|\X|$ s.t.
        \[ \lambda[i] = \max_j |\lcp_k(\X_i, \Y_j)| \]
        where
        \begin{itemize}\itemsep=2ex
          \item $\X_i$ is the suffix of $\X$ starting at $i$,
          \item $\Y_j$ is the suffix of $\Y$ starting at $j$, and
          \item $\lcp_k(X_i, Y_j)$ is the longest common prefix of $\X_i$ and $\Y_j$, while allowing $k$ mismatches.
        \end{itemize}
  \end{itemize}
\end{center}
\end{frame}


\begin{slide}
  \frametitle{Solution for $\max \lcp_k$ Problem when $k = 0$}
  \begin{itemize}\itemsep=2ex
  \item Optimal $O(n)$ time sequential algorithm.
  \item Key data structure: Generalized Suffix Tree (GST), an index data structure for a given set of sequences such that:

    \vspace{0.1cm}
       \begin{enumerate}\itemsep=1.5ex
         \vspace*{1.5ex}
       \item Every suffix is a leaf in the GST.
       \item Edges are labeled s.t. $\plabel$ of root to leaf path is the suffix.
       \item Lowest Common Ancestor (LCA) property. % : $\plabel$ of LCA of two leaves is the $\lcp$.
       \end{enumerate}
    \item<2-> Illustration of the properties of $\GST$ with two sequences: $\{ \fX, \fY\}$, where
      \vspace{1mm}

      \begin{center}
      \begin{tabular}{c}
        $\fX$ = \verb|cabana|\trx  \\
        $\fY$ = \verb|banana|\try  \\
      \end{tabular}
      \end{center}
  \end{itemize}
\end{slide}


\begin{slide}
  \frametitle{P1: Every Suffix is a Leaf}
  \includegraphics[width=1.01\linewidth]{graphics/graphics-suffix-tree.pdf}
  \begin{center}
    \begin{tabular}{c}
    $\fX$ = \verb|cabana|\trx  \\
    $\fY$ = \verb|banana|\try  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{P2: Labelling of Root to Leaf Path}
  \includegraphics[width=1.01\linewidth]{graphics/graphics-suffix-tree-leaf-path.pdf}
  \begin{center}
    \begin{tabular}{c}
    $\fX$ = \verb|cabana|\trx  \\
    $\fY$ = \verb|b|\tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|anana|\try}}  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{P3: Lowest Common Ancestor (LCA) of two Leaves}
  \includegraphics[width=1.01\linewidth]{graphics/graphics-suffix-tree-lca.pdf}
  \begin{center}
    \begin{tabular}{c}
    $\fX$ = \verb|ca|\tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|bana|}}\trx  \\
    $\fY$ = \tikz[baseline=-0.7ex,outer sep=0,inner sep=0,inner ysep=1pt]{\node[draw,fill=gtBuzz]{\verb|bana|}}\verb|na|\try  \\
   \end{tabular}
  \end{center}
\end{slide}
\begin{slide}
  \frametitle{Generalized Suffix Tree: Known Results}
  \begin{center}
    \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item Construction : $O(n)$ time (McCrieght, 1976).
    \begin{itemize}\itemsep=1.5ex
      \vspace*{0.1cm}
    \item were $n$ is the sum of the lengths of input sequences.
    \end{itemize}
    \item LCA query : $O(1)$ time (Harel and Tarjan, 1984).
    \item For any internal node $u \in \mathsf{GST}$,
      \begin{itemize}\itemsep=1.5ex
        \vspace*{0.1cm}
      \item $\sdepth(u) = |\plabel(u)|$ : $O(1)$ time.
      \item List of suffixes under $u$  : $O(\mathsf{subtree}\,\mathsf{size})$ time.
      \end{itemize}
  \end{itemize}
  \end{center}
\end{slide}

\begin{slide}
  \frametitle{Solution for the $\max \lcp_k$ Problem when $k = 0$}
  \vspace{-10mm}
  \begin{equation*}
    \lambda[i]   = \max \left\{
    \begin{array}{ll}
      \sdepth(\LCA(\Y_l, \X_i )) & \textnormal{$\Y_l$ is the nearest $\Y$ suffix on $\X_i$'s left}\\
      \sdepth(\LCA(\X_i, Y_r))  & \textnormal{$\Y_r$ is the nearest $\Y$ suffix on $\X_i$'s right}
    \end{array} \right.
  \end{equation*}
  \includegraphics[width=1.01\linewidth]{graphics/graphics-suffix-tree.pdf}
%  \[ \lambda[i] \max \{ \sdepth(\LCA(\Y_l, \X_i )), \sdepth(\LCA(\X_i, \Y_r))\]
%  $Y_l$ and $\Y_r$ nearest leaves with $\Y$'s suffix.
  \begin{center}
  \begin{itemize}\itemsep=2ex
    %\item $\max \lcp$ for any suffix $\X_i$ of $\X$ :
  \item<2-> $\max \lcp$ can be computed with two passes over the leaves : left-to-right and right-to-left.
  \end{itemize}
  \end{center}
\end{slide}

\begin{slide}
  \frametitle{Solution for the $\max \lcp_k$ Problem when $k \geq 0$}
    \vspace*{0.2cm}
    \begin{center}
      \begin{itemize}\itemsep=4ex
      \item $O(n^2k)$ solution using the following recursion:
        \begin{equation}
          \label{equation:approx-lcpk}
          |\lcp_k(\X_i, \Y_j)|   = \left\{
          \begin{array}{l l}
            |\lcp(X_i, X_j)| &
            \quad \textnormal{if $k=0$}\\
            |\lcp(X_i, X_j)| +1+|\lcp_{k-1}(\X_{i+ \lcp(X_i, Y_j) +1}, \Y_{j+\lcp(X_i, Y_j) +1})| & \quad \textnormal{if $k >0$}
          \end{array} \right.
        \end{equation}
        %where $z = |\lcp(\X_i,\Y_j)|$.
      \item $O(n \log^kn )$ time exact algorithm by Aluru, Aposotolico and Thankachan (2015) : Tricky and difficult to implement.
        %For $k > 0$ : GST is not friendly, Heuristic approaches are used.
      \item Our contribution : Simple and easy to implement algorithm with $O(n \log^k n)$ expected runtime.
      \end{itemize}
    \end{center}
\end{slide}

\begin{frame}
  \frametitle{Compact Trie of Suffixes}
  \vspace*{-3mm}
  \begin{center}
  \includegraphics[width=0.8\linewidth]{graphics/graphics-compact-suffix-trie.pdf}
  \begin{itemize}
    \item Constructed for any subset of the suffixes.
    \item Given GST, construction can be in linear time (Weiner, 1973).
    \item Height of Compact trie $\leq H$, Height of the suffix tree.
  \end{itemize}
  \end{center}
\end{frame}


%% \begin{slide}
%%    \frametitle{A B C}
%%     \begin{tabular}{ll}
%%      $X$ & \verb|GATTACAGGTATATGGATTACAACTC|\trb \\
%%      $Y$ & \verb|TTGATTACACGTAGATGATTACATGTACT|\tra \\
%%     \end{tabular}
%% %$X=$  \verb|TTGATTACACGTAGATGATTACATGTACT|
%% % %$Y=$  \verb|GATTACAGGTATATGGATTACAACTC|
%% \end{slide}


\begin{slide}
  \frametitle{Solution for the $\max \lcp_k$ Problem}
  \vspace{-0.3mm}
  \only<1>{
    \begin{center}
     Illustration of our solution with the following example
    \end{center}
    %\includegraphics[width=0.92\linewidth]{graphics/graphics-maximal-match-fwd.pdf}
    %\vspace{0.3cm}
    %An example : \\
  }
  \only<2>{
    \includegraphics[width=0.92\linewidth]{graphics/graphics-lcp-stree.pdf}
  }
  \only<3>{
    \includegraphics[width=0.92\linewidth]{graphics/graphics-lcp-stree-path.pdf}
  }
  \only<4>{
    \includegraphics[width=0.92\linewidth]{graphics/graphics-lcp-stree-chop.pdf}
  }
  \only<5>{
    \includegraphics[width=0.92\linewidth]{graphics/graphics-lcp-stree-trie.pdf}
  }
  \begin{center}
    \begin{onlyenv}<1>

   \begin{tabular}{ll}
     $\X$ & \verb|GATTACAGGTATATGGATTACAACTC|\trb \\
     $\Y$ & \verb|TTGATTACACGTAGATGATTACATGTACT|\tra \\
  \end{tabular}
   \vspace{0.5cm}

   \end{onlyenv}
  \begin{onlyenv}<2>
    \vspace{-2mm}
   \begin{tabular}{ll}
     $\X$ & \verb|GATTACAGGTATATGGATTACAACTC|\trb \\
     $\Y$ & \verb|TTGATTACACGTAGATGATTACATGTACT|\tra \\
   \end{tabular}
  \end{onlyenv}
  \begin{onlyenv}<3>
    \vspace{-2mm}
   \begin{tabular}{ll}
     $\X$ & \inhighlight{GATTACA}\verb|GGTATATG|\inhighlight{GATTACA}\verb|ACTC|\trb \\
     $\Y$ & \verb|TT|\inhighlight{GATTACA}\verb|CGTAGAT|\inhighlight{GATTACA}\verb|TGTACT|\tra \\
   \end{tabular}
  \end{onlyenv}
  \begin{onlyenv}<4->
    \vspace{-2mm}
   \begin{tabular}{ll}
     $\X$ & \inhighlight{GATTACA}\inhighx{G}\inhighlight{GTA}\verb|TATG|\inhighlight{GATTACA}\inhighx{A}\verb|CTC|\trb \\
     $\Y$ & \verb|TT|\inhighlight{GATTACA}\inhighx{C}\inhighlight{GTA}\verb|GAT|\inhighlight{GATTACA}\inhighx{T}\inhighlight{GTA}\verb|CT|\tra \\
   \end{tabular}
  \end{onlyenv}
  \end{center}
\end{slide}

\begin{slide}
  \frametitle{Solution for the $\max \lcp_k$ Problem}

  \begin{enumerate}\itemsep=3ex
    \item Initialize $\lambda[i] = 0, 1 \leq i \leq |\X|$
  \item For each internal node $u \in \GST$, construct compact trie of suffixes from the set
    \[ \{ i + \sdepth(u) + 1 \quad | \quad i \in \subtree(u) \} \]
  \item Recursively build compact tries $k-1$ times from these tries.
  \item For each internal node of the trie at level $k$, update $\lambda$ with a
    left-to-right and right-to-left pass.
  \end{enumerate}

  % Property of $k$ modified suffixes
\end{slide}

\begin{slide}
  \frametitle{Property of the Order-$h$ Universe}
  %% \begin{block}{Definition: $k$-modified suffix}
  %%   \begin{itemize}
  %%     \item Let $\TXT = \X \$_1 \Y \$_2$.
  %%     \item Let \# be a special symbol not in $\Sigma$.
  %%     \item A $k$-modified suffix is a suffix of $\TXT$ with its $k$ characters replaced by \#.
  %%     \item A suffix $\TXT[x . . .]$ modified w.r.t. the positions in a set $\Delta$ is denoted by $\TXT^\Delta [x . . .]$.
  %%   \end{itemize}
  %% \end{block}

  %\begin{block}{Definition : order-$h$ universe of suffixes of $u$}
  \begin{tcolorbox}[colback=iitbLightGray,
      colframe=iitbBlue,
      left=0mm,right=0mm,top=0mm,bottom=0mm,
      bottomrule=0.6pt,
      leftrule=0.6pt,
      rightrule=0.6pt,
      toprule=0.6pt,
      arc=0pt,outer arc=0pt,
      title={Definition : order-$h$ universe of suffixes of $u$}]
    The collection of the set of suffixes at level $h$ corresponding to an internal node $u$ in  $\GST$ is
    called an order-$h$ universe of the suffixes of u.
    \end{tcolorbox}
  %\end{block}
  \vspace{2mm}
\uncover<2->{
  %\begin{block}{Property of the order-$h$ universe}
  \begin{tcolorbox}[colback=iitbLightGray,
      colframe=iitbBlue,
      left=0mm,right=0mm,top=0mm,bottom=0mm,
      bottomrule=0.6pt,
      leftrule=0.6pt,
      rightrule=0.6pt,
      toprule=0.6pt,
      arc=0pt,outer arc=0pt,
      title={Property of the order-$h$ universe}]
    Let
    \begin{itemize}
      \item $\#$ be a character not in $\Sigma$,
      \item $\Delta$ be the set of first $h$ positions at which $\X_i$ and $\Y_j$ differ, $h \in [1,\ldots,k]$,
      \item $\X^\Delta[i...]$ be $\X_i$ whose $\Delta$ positions are replaced by $\#$ ,
      \item $\Y^\Delta[j...]$ be $\Y_j$ whose $\Delta$ positions are replaced by $\#$
    \end{itemize}
    Then,
    \begin{itemize}
      \item There exists a unique set in order-$h$ universe containing $\X^\Delta[i...]$  and $\Y^\Delta[j...]$ \ie{} $\lcp_h(\X_i, \Y_j) =  \lcp(\X^\Delta[i...], \Y^\Delta[j...])$.
    \end{itemize}
    %For any level $h \in [0, k]$ and for any two suffixes $\TXT [x . . .]$, $\TXT [y . . .]$ of $u$
    %there will be a unique set containing two modified suffixes $\TXT^\Delta[x...]$ and $\TXT^\Delta[y...]$
    %such that $\Delta$ is the set of first $h$ positions in which $\TXT[x...]$ and $\TXT[y...]$ differ
    %\[ \lcp(\TXT^\Delta[x...], \TXT^\Delta[y...]) = \lcp_h(\TXT[x...], \TXT[y...]) \]
   % in the order $h$
    %\end{block}
  \end{tcolorbox}
}
\end{slide}

%% \begin{slide}
%%   \frametitle{$k$-modified suffixes}
%%   \begin{block}{Property: $k$-modified suffixes}
%%     For any level $h \in [0, k]$ and for any two suffixes $\TXT [x . . .]$, $\TXT [y . . .]$ of $u$
%%     %\[ \lcp(\TXT^\Delta[x...], \TXT^\Delta[y...]) = \lcp_h(\TXT[x...], \TXT[y...]) \]
%%     there will be a unique set containing two modified suffixes $\TXT^\Delta[x...]$ and $\TXT^\Delta[y...]$
%%     such that $\Delta$ is the set of first $h$ positions in which $\TXT[x...]$ and $\TXT[y...]$ differ
%%    % in the order $h$
%%   \end{block}
%% \end{slide}

\begin{frame}
  \frametitle{Time Complexity}
  \vspace{-1.2mm}
  \begin{columns}[T]
    \begin{column}{0.62\linewidth}
  \only<1>{ %
    \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion-lzero.pdf} %
    } %
  \only<2>{ %
    \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion-lone.pdf} %
    } %
  \only<3->{ %
    \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion.pdf}
  } %
  \end{column}
    \begin{column}{0.37\linewidth}
      \vspace{0.3cm}
      Let
      \begin{itemize}
      %\item $\tau$ be long enough.
        \item $m = |$An Internal Node$|$.
        \item $H = $ Height of the GST.
      \end{itemize}
      \uncover<4->{ %
        Then
          \[ \sum_{j = 0}^{k} O(mH^{\,j}) = O(mH^k) \]
      }
      \uncover<5>{%
        Since $m \leq n$, \\
        $E(H) = \log n$ (Devroye '92), \\
        and the passes take linear time,
        \[ O(n \log^k n) \]
        is the expected runtime.
      }
  \end{column}
  \end{columns}
\end{frame}

%% \subsection{$k$-mismatch LCS}
%% \begin{frame}
%%   \frametitle{Problem Definition -- $k$-mismatch Longest Common String }
%%     \vspace*{0.5cm}
%% \begin{center}
%%   \begin{itemize}\itemsep=4ex
%%     \vspace*{0.1cm}
%%   \item Let $\Sigma$ be the alphabet set.
%%   \item Given
%%     \begin{itemize} \itemsep=2ex
%%       \vspace*{0.1cm}
%%     \item Two stings $\X$ and $\Y$ s.t. $|\X| + |\Y| = n$
%%     \item Mismatch threshold $k \geq 0$
%%     \end{itemize}
%%   \item Report
%%     \begin{itemize}
%%     \item AXX
%%     \end{itemize}
%%   \end{itemize}
%% \end{center}
%% \end{frame}


\subsection{$k$-mismatch LCS}


\subsection{Application : Phylogenetic Inference}

%% \begin{slide}
%%   \frametitle{EColi Time}
%% \begin{center}
%% \includegraphics[width=\linewidth]{graphics/exact_ecoli_plot3.pdf}
%% \end{center}
%% \end{slide}

\begin{slide}
  \frametitle{$\ACS_k$ for construction Phylogenetic Tree}
  \begin{itemize}\itemsep=3ex
  \item  Phylogenetic Tree : A tree representation of the evolutionary relationships between $r$ species.
    \only<2->{
  \item<2-> Multiple Sequence Alignment (MSA) approaches most accurate, but are computationally expensive.
    \vspace{0.2cm}
    \begin{itemize}
    \item Pipeline : MSA $\rightarrow$ $r \times r$ Distance Matrix $\rightarrow$ Tree Inference.
    \end{itemize}
  \item<3-> Recently, functions of $\ACS$ (Ulitsky~\etal{}, 2006) and $\ACS_k$ (Leimeister~\etal{}, 2014) proposed as the distance measure instead of using MSA.
    \[ \ACS_k(\X, \Y) = \frac{1}{|\X|}\sum_{i=1}^{|\X|}\lambda[i] \]
  \item<3-> Our Contribution: Compute $\ACS_k$ exactly.
    }
  \end{itemize}
  \begin{center}
  \only<1>{
    \includegraphics[scale=0.4]{graphics/primates-tree.pdf}
  }
  \end{center}
\end{slide}

\begin{slide}
  \frametitle{Phylogenetic Tree Construction for \textit{27 Primates} Dataset}
\begin{center}
  \includegraphics[width=\linewidth]{graphics/exact_primates_plot3.pdf}

  \begin{itemize}\itemsep=2ex
  \item None of the other alignment-free methods can recover the exact tree.
  \item Runtime grows exponentially. %, but small value of $k$ is expected to be used.
  \end{itemize}
\end{center}
\end{slide}

%% \begin{slide}
%%   \frametitle{Primates Time}
%% \begin{center}
%% \includegraphics[scale=0.5]{graphics/primates-tree.pdf}
%% \end{center}
%% \end{slide}

%\begin{slide}
%  \frametitle{Greedy Algorithm For Large Dataset}
%\end{slide}

%% \begin{slide}
%%   \frametitle{Greedy Algorithm}
%%   \begin{center}
%%     \includegraphics[width=\linewidth]{graphics/primates_plot.pdf}
%%   \end{center}
%% \end{slide}


%% \begin{slide}
%%   \frametitle{Greedy Algorithm for \textit{Roseobacter} dataset}
%%   \begin{center}
%%     \includegraphics[width=\linewidth]{graphics/roseobacter-tree.pdf}
%%   \end{center}
%% \end{slide}

\begin{slide}
  \frametitle{ALFRED-G : Greedy Algorithm for $\ACS_k$}
  \begin{itemize}
  \item For large datasets, computing exact $\ACS_k$ is infeasible.
  \item Greedy Algorithm : Compute $|\lcp_1(\cdot, \cdot)|$ and extend the match to allow $k-1$ more mismatches.
  \item
    \only<1>{
      Recovers the complete tree for \textit{27 Primates} dataset.
    }
    \only<2>{
      Result for \textit{Roseobacter} dataset (32 organisms; total size $\approx$1MB):
    }
    \only<3>{
      Result for \textit{Balibase} dataset (218 set of protein sequences):
    }
    \vspace{5mm}
    \begin{center}
      \only<1>{
        \vspace{4cm}
      }
    \only<2>{
      \includegraphics[width=0.7\linewidth]{graphics/roseobacter_plot.pdf}
    }
    \only<3>{
      \includegraphics[width=0.7\linewidth]{graphics/balibase_plot.pdf}
    }
  \end{center}
  \end{itemize}
\end{slide}

%% \begin{slide}
%%   \frametitle{Greedy Algorithm}
%%   \begin{center}
%%     \includegraphics[width=\linewidth]{graphics/balibase_plot.pdf}
%%   \end{center}
%% \end{slide}

\subsection{$k$MCS Problem}
\begin{frame}
  \frametitle{Problem Definition -- $k$MCS}
    \vspace*{0.5cm}
\begin{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  %\item Let $\Sigma$ be the alphabet set.
  \item Given
    \begin{itemize} \itemsep=2ex
      \vspace*{0.1cm}
      \item Collection $\D=\{S_1,S_2,\dots, S_m\}$ of $m$ sequences, drawn from the alphabet set $\Sigma$, with $\sum_i |S_i| = N$,
      \item Length threshold $\phi > 0$ and
      \item Mismatch threshold $k \geq 0$.
    \end{itemize}
  \item Report
    \begin{itemize}
    \item All $k$-mismatch maximal common substrings (MCS) of length  $\geq \phi$ of any pair of sequences in $\D$.
    \end{itemize}
  \end{itemize}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Properties of Output Pairs}
  \begin{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{1cm}
  \item Output produced as pairs: $(S_a[x..], S_b[y..])$.
  \item<2-> A pair $(S_a[x..], S_b[y..])$ is a valid output if:
    \begin{enumerate} \itemsep=2ex
      \vspace*{0.2cm}
    \item \textbf{Length Threshold Condition : } $S_a[x..]$ and $S_b[y..]$ share a common prefix of length $\geq \phi$ while allowing $k$ mismatches.
      \item \textbf{Maximality Condition : } $x = 1$ or $y = 1$ or $S_a[x - 1] \neq S_b[y - 1]$.
      \item \textbf{Sequence Identity Condition : } $a \neq b$.
      \end{enumerate}
  \end{itemize}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Sequential Algorithm for $k$MCS}
  \begin{center}
    \begin{itemize}\itemsep=2ex
    \item Condition of $k$ mismatches satisfied by the property of the order-$k$ universe.
    %\item Maximality and Sequence Identity conditions satisfied during pair generation as below.
    \item Pair Generation for a trie at level $k$, $T_k^{\,z}$ :
      \vspace{3mm}

      \begin{tabular}{ll} For $v \in T_k^{\,z}$ & s.t. \\
      & $\match + \sdepth(v) \geq \phi$ and \\
      & $\match + \sdepth(parent(v)) < \phi$ \\ \end{tabular}
   \vspace*{0.2cm}
   \begin{enumerate}\itemsep=2.5ex
   \item Sort the suffixes into $|\Sigma|+1$ buckets.
   \item Sort suffixes in each bucket by sequence identifier.
   \item Generate suffix pairs satisfying maximality conditions.
   \end{enumerate}
   \item Takes $O((n + \occ) \log^k n)$ time in expectation.
   %\item With additional  $|\Sigma|^k$ buckets, $O(n^k \log^k n + \occ)$ expected time.
   \end{itemize}
  \end{center}
\end{frame}



\subsection{Parallel Algorithm for $k$MCS Problem}
\begin{slide}
  \frametitle{Parallel Algorithm for $k$MCS Problem}
  \begin{itemize}\itemsep=3ex
  \item Motivation:
    \vspace{1mm}
    \begin{itemize}\itemsep=1ex
    %\item Clustering and assembly applications in computational biology.
    \item Error rates of high throughput sequencing datasets necessitate mismatches.
    \item Size of high throughput sequencing datasets necessitate parallel algorithms.
    \end{itemize}
  \item For $k = 0$, distribute GST's internal nodes with $\sdepth(\cdot) > \phi$.
  \item For $k > 0$, using GST is not straight forward; Heuristic approaches are used.
  \item Our contribution : A provably efficient parallel algorithm with
    \[ O\left(\left(\frac{N}{p} \log N + \occ\right) \log^k N\right) \]
    expected runtime ($p$ is the number of processors).
  \end{itemize}
\end{slide}

\begin{frame}
  \frametitle{Length of Maximal Match Segments}
  \begin{center}
    \includegraphics[width=\linewidth]{graphics/graphics-maximal-match.pdf}
  \end{center}
  \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
  \item A $k$ mismatch maximal common substring has $k + 1$ maximal matches.
  \item Longest maximal match is at least
    \[ \tau = \left\lceil \frac{\phi - k}{ k + 1} \right\rceil \]
  \item<2-> Find the $\geq \tau$ length maximal match segment first.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Our Approach}
  \vspace*{5mm}
 \only<1>{
  \begin{center}
    \includegraphics[width=\linewidth]{graphics/graphics-maximal-match-olap.pdf}
  \end{center}
  }
\only<2>{
  \begin{center}
    \includegraphics[width=\linewidth]{graphics/graphics-maximal-match-fwd.pdf}
  \end{center}
  }
\only<3>{
  \begin{center}
    \includegraphics[width=\linewidth]{graphics/graphics-maximal-match-rev.pdf}
  \end{center}
}
\only<4->{
  \begin{center}
    \includegraphics[width=\linewidth]{graphics/graphics-maximal-match-fwdrev.pdf}
  \end{center}
    }
    \vspace*{0.3cm}
    \begin{itemize}\itemsep=3ex
    \item<2-> Start with $\geq \tau$ length match, search towards the right of the match.
    \item<3-> Start with $\geq \tau$ length match, search towards the left of the match.
    \item<4-> For every rightward search, search leftward ($k+1$ possible ways).
    \item<5-> Using distributed GST, we search simultaneously in parallel.
    \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Primary Nodes}
 \vspace{-4mm}
  \only<1>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{graphics/graphics-gst-main.pdf}
  \end{center}
  }
  \only<2>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{graphics/graphics-gst-primary.pdf}
  \end{center}
  }
  \only<3>{
  \begin{center}
    \includegraphics[width=0.8\linewidth]{graphics/graphics-gst-distribute.pdf}
  \end{center}
  }
 \vspace{-4mm}
  \begin{center}
  \begin{itemize}
  \item<1-> Construct distributed GST of $\mathcal{D} = \{S_1,\ldots,S_m\}$.
  \item<2-> Identify Primary Nodes: $u$ such that
    \[ \sdepth(u) \geq \tau > \sdepth(parent(u)) \]
  \item<3-> Distribute Primary Nodes (Assuming the \# of $\tau$-length strings $\leq N/p$ ).
  \end{itemize}
  \end{center}
\end{frame}
%% \begin{slide}
%%   \frametitle{Rightward Search}
%%   \vspace{-0.3mm}
%%   \only<1-2>{
%%     \begin{center}
%%       Rightwards Search
%%     \end{center}
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-maximal-match-fwd.pdf}

%%     \vspace{0.3cm}

%%     Example with four sequences: \\
%%   }
%%   \only<3>{
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-fwd-stree.pdf}
%%   }
%%   \only<4>{
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-fwd-stree-path.pdf}
%%   }
%%   \only<5>{
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-fwd-stree-chop.pdf}
%%   }
%%   \only<6>{
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-fwd-stree-trie.pdf}
%%   }
%%   \begin{center}
%%     \begin{onlyenv}<2>

%%    \begin{tabular}{ll}
%%     $S_1$ & \verb|ATAAATTGATTACATGTAGAT|\tra \\
%%     $S_2$ & \verb|GATTACAGGTATATGACT|\trb \\
%%     $S_3$ & \verb|ATCAATGGATTACACTCACG|\trc \\
%%     $S_4$ & \verb|GATCGATTACAAGTAC|\trd \\
%%    \end{tabular}
%%    \vspace{0.5cm}

%%    $\phi = 14, k = 2 \Rightarrow \tau = 4$
%%    \end{onlyenv}
%%   \begin{onlyenv}<3>
%%     \vspace{-2mm}
%%    \begin{tabular}{ll}
%%     $S_1$ & \verb|ATAAATTGATTACATGTAGAT|\tra \\
%%     $S_2$ & \verb|GATTACAGGTATATGACT|\trb \\
%%     $S_3$ & \verb|ATCAATGGATTACACTCACG|\trc \\
%%     $S_4$ & \verb|GATCGATTACAAGTAC|\trd \\
%%    \end{tabular}
%%   \end{onlyenv}
%%   \begin{onlyenv}<4>
%%     \vspace{-2mm}
%%    \begin{tabular}{ll}
%%     $S_1$ & \verb|ATAAATT|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
%%     $S_2$ & \verb|       |\inhighlight{GATTACA}\verb|GGTATATGACT|\trb \\
%%     $S_3$ & \verb|ATCAATG|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
%%     $S_4$ & \verb|   GATC|\inhighlight{GATTACA}\verb|AGTAC|\trd \\
%%    \end{tabular}
%%   \end{onlyenv}
%%   \begin{onlyenv}<5->
%%     \vspace{-2mm}
%%    \begin{tabular}{ll}
%%     $S_1$ & \verb|ATAAATT|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
%%     $S_2$ & \verb|       |\inhighlight{GATTACA}\verb|GGTATATGACT|\trb \\
%%     $S_3$ & \verb|ATCAATG|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
%%     $S_4$ & \verb|   GATC|\inhighlight{GATTACA}\verb|AGTAC|\trd \\
%%    \end{tabular}
%%   \end{onlyenv}
%%   \end{center}
%% \end{slide}

\begin{frame}
  \frametitle{Rightward Search - Time Complexity}
  \vspace{-1.2mm}
  \begin{columns}[T]
    \begin{column}{0.62\linewidth}
  \only<1>{ %
    \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion-lzero.pdf} %
  } %
 % \only<2>{ %
 %   \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion-lone.pdf} %
 % } %
  \only<2->{ %
    \includegraphics[width=\linewidth]{graphics/graphics-algo-recursion.pdf}
  } %
  \end{column}
    \begin{column}{0.37\linewidth}
      \vspace{0.3cm}
      Let
      \begin{itemize}
      \item $\tau$ be long enough.
        \item $m = |$A Primary Node$|$.
        \item $H = $ Height of the GST.
      \end{itemize}
      \uncover<3->{ %
        Then
          \[ \sum_{j = 0}^{k} O(mH^{\,j}) = O(mH^k) \]
      }
      \uncover<3>{%
        Since $m \leq N/p$ and
         $E(H) = \log N$ (Devroye '92),
        \[ O((N/p) \log^k N) \]
        is the expected runtime.
      }
  \end{column}
  \end{columns}
\end{frame}

%% \begin{slide}
%%   \frametitle{Leftward Search}
%%   \only<1>{
%%     \begin{center}
%%       Leftward Search
%%     \end{center}
%%     \includegraphics[width=0.92\linewidth]{graphics/graphics-maximal-match-rev.pdf}
%%     \vspace{0.5cm}

%%     In our example,  \\
%%     }
%%  \only<2->{
%%   \includegraphics[width=0.9\linewidth]{graphics/graphics-rev-stree-trie.pdf}
%%  }
%%  \begin{center}

%%    \begin{tabular}{ll}
%%     $S_1$ & \verb|ATA|\inhighlight{AAT}\verb|T|\inhighlight{GATTACA}\verb|TGTAGAT|\tra \\
%%     $S_3$ & \verb|ATC|\inhighlight{AAT}\verb|G|\inhighlight{GATTACA}\verb|CTCACG|\trc \\
%%    \end{tabular}
%%  \end{center}
%% \end{slide}

\begin{frame}
  \frametitle{\only<1>{Leftward Search}\only<2->{Right-Left Search}}
  \only<1>{ %
    \begin{center}
      \includegraphics[width=0.8\linewidth]{graphics/graphics-algo-recursion-reverse.pdf} %
    \end{center}
  } %
  \only<2>{ %
  \vspace*{2cm}
    \begin{center}
      Right-Left Search
    \end{center}
    \begin{center}
    \includegraphics[width=0.8\linewidth]{graphics/graphics-maximal-match-fwdrev.pdf} %
    \end{center}
  }
  \only<3->{ %
    \begin{center}
    \includegraphics[width=0.8\linewidth]{graphics/graphics-algo-recursion-reversejx.pdf} %
    \end{center}
  }
    \begin{center}
  \only<1>{
    Parallel runtime for construction remain $O((N/p) H^k)$.
  }
  \only<3>{
     Parallel runtime for construction remain $O((N/p) H^k)$.
  }
  \only<4>{
      Each output pair processed $O(H^k)$ times. Hence $O((N/p + \occ) H^k)$ time.
  }
    \end{center}
\end{frame}
\begin{frame}
  \frametitle{Parallel Algorithm : Implementation Details}
  \vspace{0.5cm}
  \begin{center}
    \begin{itemize}\itemsep=4ex
    \vspace*{0.1cm}
    \item Use distributed Suffix Array (SA) and Longest Common Prefix (LCP) arrays (Flick and Aluru, 2015) for GST for $\D$ and $\overleftarrow{\D}$ .
    \item Tries represented by corresponding SA and LCP arrays.
    \item Distribution via shifting SA, LCP appropriately.
    \item Tries construction by \emph{all2all} query on Inverse Suffix Array (ISA).
      \begin{itemize}
        \vspace{1ex}
      \item $O\left( \log^{k+1} N\right)$ expected rounds of global communications.
      \end{itemize}
    \end{itemize}
  \end{center}
\end{frame}
%\begin{frame}
%  \frametitle{Communication Complexity}
%\end{frame}
\begin{frame}
  \frametitle{Parallel Algorithm : Experiments}
  \vspace*{0.1cm}
  \begin{itemize}\itemsep=3ex
  \item Datasets
    \vspace{2mm}
  \begin{center}
\renewcommand{\arraystretch}{1.4}
\begin{tabular}{lrrr}
\hline
            & D1          & D2            & D3          \\
\hline
Type        & RNA         & DNA           & RNA         \\
Organism    & H. sapiens  & S. cerevisiae & H. sapiens  \\
Sequencer   & NextSeq 500 & HiSeq 2000    & HiSeq 2500  \\
No. Sequences   & 60 M  & 18 M    & 272 M \\
Sequence Length & 75          & 101           & 151         \\
Input Size  & 3.2 Gbp   & 1.7 Gbp  & 35.1 Gbp   \\
\hline
\end{tabular}
  \end{center}
\item Run on Infiniband Cluster with each node having two 2.0GHz 8-core Intel Xeon E5-2650 and 128GB memory; Up to 64 nodes (1024 cores) used.
  \end{itemize}
\end{frame}
%% \begin{frame}
%%   \frametitle{Experiments : Pre-processing}
%%   \vspace*{0.5cm}
%%   \begin{center}
%% \renewcommand{\arraystretch}{1.3}
%%   \begin{tabular}{lrrr}
%%     \hline
%% \multicolumn{4}{c}{{\textsc{Dataset Sizes before/after pre-processing}}} \\
%% \hline
%%            & D1         & D2        & D3          \\
%% \hline
%% No. Reads  Before PP  & 60,100,561  & 18,415,332    & 272,462,716 \\
%% No. Reads  After PP & 21,682,850 & 8,263,882 & 116,295,542 \\
%% Size Before PP  & 4.507Gbp    & 1.860Gbp      & 38.417Gbp   \\
%% Size After PP  & 1.626Gbp   & 0.835Gbp  & 17.560Gbp   \\
%% Input Size & 3.252Gbp   & 1.669Gbp  & 35.120Gbp   \\
%% \hline
%% \end{tabular}
%%   \end{center}
%% \end{frame}

\begin{frame}
  \frametitle{Results for D1}

  \vspace{4mm}
\renewcommand{\arraystretch}{1.3}
\begin{center}
\begin{tabular}{l|r|r|r|r|r}
\hline
 & \multicolumn{5}{c}{$\tau = 17$} \\
 \cline{2-6}
 & \multicolumn{2}{c|}{$k = 1$}     & \multicolumn{2}{c|}{$k = 2$} & $k = 3$ \\
 \cline{2-6}
No. of & Runtime & Relative & Runtime  & Relative & Runtime \\
Cores  & (sec)   & Speedup  & (sec)    & Speedup  & (sec) \\
\hline
128    & \innode{tx1}{-1.5ex}1607.69\innode{tx2}{0ex} & 1.00X    & \innode{ty1}{-1.5ex}14991.90\innode{ty2}{0ex} & 1.00X    & -- \\
256    & 826.71\innode{tx3}{0ex} & 1.94X    & 7952.84\innode{ty3}{0ex} & 1.88X    & -- \\
512    & 406.32\innode{tx4}{0ex} & 3.95X    & 4317.62\innode{ty4}{0ex} & 3.47X    & -- \\
1024   & 213.29\innode{tx5}{0ex} & 7.53X    & 2338.47\innode{ty5}{0ex} & 6.41X    & 29676.70 \\
\hline
\end{tabular}
\end{center}
\only<1>{
\begin{tikzpicture}[remember picture,overlay]
  \fill[gtBuzz] (tx1.north west) rectangle (tx5.south east);
  \fill[gtBuzz] (ty1.north west) rectangle (ty5.south east);
\end{tikzpicture}
}
\only<2>{
\begin{tikzpicture}[remember picture,overlay]
  \fill[gtBuzz] (tx1.north west) rectangle (tx4.south east);
  \fill[gtBuzz] (ty1.north west) rectangle (ty4.south east);
\end{tikzpicture}
}
\end{frame}
\begin{frame}
  \frametitle{D1 vs. D2}
  \vspace*{-1mm}
  \begin{itemize}
  \item Scalability of D1 and D2:

    \begin{center}
      \includegraphics[width=0.75\linewidth]{graphics/Rplot01.pdf}
    \end{center}

  \item For $k = 1$, D3 takes $\approx$7.47 hours and $\approx$4.17 hours with 512 and 1024 cores respectively.
  \end{itemize}
\end{frame}

%% \begin{frame}
%%   \frametitle{Results for D3 (k = 1)}

%%   \vspace{2cm}
%% \renewcommand{\arraystretch}{1.5}
%% \begin{center}
%% \begin{tabular}{l|r|r}
%% \hline
%%  No. of & Time     & Relative \\
%%  Cores  & (sec)    & Speedup  \\
%% \hline
%%  512    & 26914.10 & 1.00X    \\
%%  1024   & 15031.40 & 1.79X    \\
%% \hline
%% \end{tabular}
%% \end{center}
%% \end{frame}

