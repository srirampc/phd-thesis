\section{Introduction}
\begin{slide}
  \frametitle{Introduction : Motivation}
  \begin{itemize}\itemsep=3.6ex
  \item Computational Biology
    \begin{itemize}\itemsep=1ex
      \vspace{2mm}
    \item Build computational models that accurately reflect biological processes.
    \item Discover relationships between biologically meaningful entities, for e.g., sequences and genes.
    \end{itemize}
  \item Motivation
    \begin{itemize}\itemsep=1ex
      \vspace{2mm}
    \item Models limited by size of the data (OR) number of variables in the domain.
    \item Methods to analyze large scale data and thus accelerate biological discoveries.
    \end{itemize}
  \item Focus : three problems over two key areas
    \begin{itemize}\itemsep=1ex
      \vspace{2mm}
    \item Approximate sequence matching (Goal: Find \textit{closely related} sequences).
    \item Reverse-engineering gene networks (Goal: Find \textit{potential gene} interactions).
    \end{itemize}
  \end{itemize}
\end{slide}

\begin{slide}
  \frametitle{Introduction : Approximate Sequence Matching}
  \begin{tcolorbox}[colback=iitbLightGray,
      colframe=iitbBlue,
      left=0mm,right=0mm,top=0mm,bottom=0mm,
      bottomrule=0.6pt,
      leftrule=0.6pt,
      rightrule=0.6pt,
      toprule=0.6pt,
      arc=0pt,outer arc=0pt,
      title={$\max \lcp_k$ Problem}]
    Given
    \begin{itemize} \itemsep=1ex
      \vspace{0.1cm}
    \item Two sequences $\X$ and $\Y$, drawn from alphabet $\Sigma$, $|\X| + |\Y| = n$
    \item Mismatch threshold $k \geq 0$,
    \end{itemize}
    Compute for each $i, 1 \leq i \leq |\X|$
    \begin{itemize}
    \item Longest prefix of $\X$ starting at $i$ that matches with a substring of $\Y$ while allowing $k$ mismatches.
    \end{itemize}
  \end{tcolorbox}
  \begin{onlyenv}<1>
    \vspace{1cm}
    \begin{center}
   \begin{tabular}{ll}
     $\X$ & \inhighlight{GATTACA}\verb|GGTATATGGATTACA|\verb|ACTC| \\
     $\Y$ & \verb|TT|\inhighlight{GATTACA}\verb|CGTAGAT|\inhighlight{GATTACA}\verb|TGTACT| \\
   \end{tabular}
    \end{center}
   \vspace{1cm}
  \end{onlyenv}
  \begin{onlyenv}<2>
   \vspace{1cm}
    \begin{center}
   \begin{tabular}{ll}
     $\X$ & \inhighlight{GATTACA}\inhighx{G}\inhighlight{GTA}\verb|TATG|\verb|GATTACAACTC| \\
     $\Y$ & \verb|TT|\inhighlight{GATTACA}\inhighx{C}\inhighlight{GTA}\verb|GAT|\inhighlight{GATTACA}\inhighx{T}\inhighlight{GTA}\verb|CT| \\
   \end{tabular}
    \end{center}
   \vspace{1cm}
  \end{onlyenv}
\only<3>{
\vspace{-2mm}
  \begin{itemize}\itemsep=1ex
  \item State of the art:  $O(n \log^k n)$ time exact solution (heavy path decomposition).
  \item Contributions:
    \begin{itemize}\itemsep=1ex
    \item Simple, easy to implement solution in $O(n \log^k n)$ expected time.
    \item Demonstrate its applicability for phylogenetic tree construction.
    \item A faster, greedy solution applicable for large datasets.
    \end{itemize}
  \end{itemize}
}
\end{slide}

\begin{slide}
  \frametitle{Introduction : Approximate Sequence Matching}
  \begin{tcolorbox}[colback=iitbLightGray,
      colframe=iitbBlue,
      left=0mm,right=0mm,top=0mm,bottom=0mm,
      bottomrule=0.6pt,
      leftrule=0.6pt,
      rightrule=0.6pt,
      toprule=0.6pt,
      arc=0pt,outer arc=0pt,
      title={$k$MCS Problem}]
    Given
    \begin{itemize}\itemsep=1ex
    \item Collection of $m$ sequences $\D = \{S_1,S_2,\ldots,S_m\}$, $\sum_i |S_i| = N$
    \item Length threshold $\phi > 0$
    \item Mismatch threshold $k \geq 0$
    \end{itemize}
    Report
    \begin{itemize}
    \item All $k$-mismatch maximal common substrings (MCS) of length  $\geq \phi$ of any pair of sequences in $\D$.
    \end{itemize}
  \end{tcolorbox}
\vspace{-2mm}
  \uncover<2>{
  \begin{itemize}\itemsep=1ex
  \item State of the art: Heuristic solutions with no known runtime bounds.
  \item Contributions:
    \begin{itemize}\itemsep=1ex
      \vspace{0.5ex}
    \item Sequential algorithm in $O(N \log^k N + \occ)$ expected time.
    \item Parallel algorithm in $O(((N/p) + \occ) \log^k N)$ expected time.
    \end{itemize}
  \end{itemize}
  }
\end{slide}

\begin{slide}
  \frametitle{Introduction : Genome-scale Gene Networks}
  \begin{tcolorbox}[colback=iitbLightGray,
      colframe=iitbBlue,
      left=0mm,right=0mm,top=0mm,bottom=0mm,
      bottomrule=0.6pt,
      leftrule=0.6pt,
      rightrule=0.6pt,
      toprule=0.6pt,
      arc=0pt,outer arc=0pt,
      title={Reverse-engineering Genome-scale Gene Networks}]
    Given
    \begin{itemize}\itemsep=1ex
    \item  $D$, an $n \times m$ matrix, gene expression values of $n$ genes across $m$ expts.
    \end{itemize}
    Construct
    \begin{itemize}
      \item A network of $n$ genes, where an edge $(g_i, g_j)$ represents a potential interaction between the genes.
    \end{itemize}
  \end{tcolorbox}
  \vspace{-2mm}
  \uncover<2>{
  \begin{itemize}\itemsep=1ex
  \item State of the art: Three methods known to work at genome-scale with contradicting claims of effectiveness.
  \item Contributions:
    \begin{itemize}\itemsep=1ex
      \vspace{0.5ex}
    \item Largest known collection of microarray datasets (11,760 expts.).
    \item Comparative study of the three known methods.
    \item MapReduce solution to the best performing solution, \textit{TINGe}.
    \end{itemize}
  \end{itemize}
  }
\end{slide}
