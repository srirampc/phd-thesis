# README #

PhD Thesis, Pre-synopsis and Defense presentation latex files. 

### Thesis ###

Thesis files are present in thesis/ folder. To build it do "make". A copy of the output file is made available as "thesis/thesis.pdf"
sciitb.sty follows most of the guidelines suggested by the IIT Bombay Rules.

### Defense ###

Defense presentation is present in defense/ folder. This requires "Concourse" and "Minion Pro" font families. A copy of the defense presentation is made available as "defense/defense.pdf"

